<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class billingClass extends DbAccess {
		public $view='';
		public $name='billing';
		
			
		function show(){

	        $dateFrom = $_REQUEST['from_date'] ? " AND `date_created` ='" . $_REQUEST['from_date'] . "'" : '';
	        $dateTo   = $_REQUEST['to_date'] ? " AND `date_created` ='" . $_REQUEST['to_date'] . "'" : '';
	        $bydate   = $dateFrom ? $dateFrom : $dateTo;
	        $date     = ($dateFrom && $dateTo) ? " AND `date_created` BETWEEN '" . $_REQUEST['from_date'] . "' AND '" . $_REQUEST['to_date'] . "'" : $bydate;

	        $today = $_REQUEST['day']=="today" ? " AND `date_created` LIKE '".date('Y-m-d')."%'" : '';

			if($_REQUEST['search']){
				$uquery = "SELECT * FROM `bill_fare` WHERE 1 $date $today ORDER BY `id` DESC";
			}
			elseif($_SESSION['utype']=='Administrator' ||($_SESSION['utype']=='Employee' && $_SESSION['post']=='2')){
				$uquery = "SELECT * FROM `bill_fare` WHERE `status`=1 $today ORDER BY `id` DESC";

			}elseif($_SESSION['utype']=='Employee'){

				$uquery = "SELECT * FROM `bill_fare` WHERE 1 AND `date_created` LIKE '".date('Y-m-d')."%' ORDER BY `id` DESC LIMIT 20";
			}
			// $uquery ="select * from users where utype='Employee'";
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row=count($uresults);	
			$_SESSION['country'] = $uquery;
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
				if($_SESSION['utype']=='Employee' ){
				$query = $uquery;
				}else{
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				}
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 			
		}

		function addnew(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM `bill_items` WHERE `bill_no` = '".$_REQUEST['bill_no']."' AND `cust_mobile`='".$_REQUEST['mobile']."' AND `bill_id`='".$_REQUEST['id']."' ";
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save(){

			$customer_name = ucwords($_REQUEST['customer_name']);
			$mobile = $_REQUEST['mobile'];
			$dob = $_REQUEST['dob'];

			/*=========Array============*/
			$items = $_REQUEST['items'];
			$item_name = $_REQUEST['item_name'];
			$qty = $_REQUEST['qty'];
			$rate = $_REQUEST['rate'];
			$discount = $_REQUEST['discount'];
			$amount = $_REQUEST['amount'];
			/*=========================*/

			$total_amt = $_REQUEST['total_amt'];

			$tax_percent = $_REQUEST['select_tax'];
			$total_tax = $_REQUEST['total_tax'];

			$disc_percent = $_REQUEST['select_disc'];
			$total_disc = $_REQUEST['total_disc'];

			$grand_total = $_REQUEST['grand_total'];
			$select_paymode = $_REQUEST['select_paymode'];
			$card_number = $_REQUEST['card_number'];
			$total_item = $_REQUEST['total_item'];
			$total_qty = $_REQUEST['total_qty'];
			$financial_year = $_SESSION['fyear'];
			$token_no = ($this->token_no()+1);
			$id = $_REQUEST['id'];
			$bill_no = $_REQUEST['bill_id'];
			$bill_date = $_REQUEST['bill_date'];
			

			if(!$bill_no){

			 	 $query = mysql_query("INSERT INTO `bill_fare`(`customer_name`, `mobile`, `dob`, `total_item`, `total_qty`, `tax_percent`, `total_tax`, `disc_percent`, `total_discount`, `total_amount`, `grand_total`, `financial_year`, `payment_mode`, `card_number`, `date_created`, `date_modify`, `emp_id`) VALUES ('".$customer_name."','".$mobile."','".$dob."','".$total_item."','".$total_qty."','".$tax_percent."','".$total_tax."','".$disc_percent."','".$total_disc."','".$total_amt."','".$grand_total."','".$financial_year."','".$select_paymode."','".$card_number."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', '".$_SESSION['adminid']."' )");


			$last_id = mysql_insert_id();
			$bill_no = "KSFW".$financial_year.$last_id;
		

			 $update = mysql_query("UPDATE `bill_fare` SET `bill_no`='".$bill_no."', `token_no`='".$token_no."' WHERE `id`='".$last_id."'");
			if($last_id>0){
			for ($i=0; $i<$total_item ; $i++) { 
				$sql = "INSERT INTO `bill_items`(`bill_id`, `bill_no`, `cust_mobile`, `product_id`, `product_name`, `qty`, `rate`, `discount`, `amount`, `financial_year`, `bill_date`, `date_created`) VALUES ('".$last_id."', '".$bill_no."', '".$mobile."', '".$items[$i]."', '".$item_name[$i]."', '".$qty[$i]."', '".$rate[$i]."', '".$discount[$i]."', '".$amount[$i]."', '".$financial_year."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";

				mysql_query($sql);
			}

			/*===================Activity Log====================*/
			$activity = "Add New Bill (".$bill_no.") of total amount:".$grand_total;

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/ 
			$_SESSION['alertmessage'] = ADDNEWRECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;	
			$_SESSION['bno'] = $bill_no;	
			// $_SESSION['print_bill'] = '1';


			
			echo "<script>window.open('index.php?control=billing&task=print_bill&bno=".$bill_no."','_blank')</script>";
			echo "<script>window.open('index.php?control=billing&task=addnew','_self')</script>";
			//header("location:index.php?control=billing&task=print_bill&bno=$bill_no");
			// header("location:index.php?control=billing&task=addnew");
			// header("location:index.php?control=billing&task=view_bill&bno=$bill_no");
		
		} 
			}else{
				/*===============Update Bill================*/

				$update = mysql_query("UPDATE `bill_fare` SET `customer_name`='".$customer_name."', `dob`='".$dob."', `total_item`='".$total_item."',`total_qty`='".$total_qty."',`tax_percent`='".$tax_percent."',`total_tax`='".$total_tax."',`disc_percent`='".$disc_percent."',`total_discount`='".$total_disc."',`total_amount`='".$total_amt."',`grand_total`='".$grand_total."',`payment_mode`='".$select_paymode."',`card_number`='".$card_number."',`date_modify`='".date('Y-m-d H:i:s')."',`emp_id`='".$_SESSION['adminid']."' WHERE `bill_no`='".$bill_no."' AND `mobile`='".$mobile."' AND `id`='".$id."'");

				/*==========Delete Old record==========*/
				$delete_old = mysql_query("DELETE FROM `bill_items` WHERE `bill_no`='".$bill_no."' AND `cust_mobile`='".$mobile."' AND `bill_id`='".$id."'");
				/*==========Rest Auto Increment==========*/
				$sql1 = mysql_query("ALTER TABLE `bill_items` AUTO_INCREMENT = 1");

				/*=============Insert New Data=========*/
			for ($i=0; $i<$total_item ; $i++) { 
				$sql = "INSERT INTO `bill_items`(`bill_id`, `bill_no`, `cust_mobile`, `product_id`, `product_name`, `qty`, `rate`, `discount`, `amount`, `financial_year`, `bill_date`, `date_created`) VALUES ('".$id."', '".$bill_no."', '".$mobile."', '".$items[$i]."', '".$item_name[$i]."', '".$qty[$i]."', '".$rate[$i]."', '".$discount[$i]."', '".$amount[$i]."', '".$financial_year."', '".$bill_date."', '".date('Y-m-d H:i:s')."');";

				mysql_query($sql);
			} 
			/*===================Activity Log====================*/
			$activity = "Update Bill (".$bill_no.") of total amount:".$grand_total;

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/ 
			$_SESSION['alertmessage'] = UPDATERECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;	

			header("location:index.php?control=billing&task=view_bill&bno=$bill_no");
			
			}

		}

		function view_bill(){
			
		 $query_com ="SELECT * FROM `bill_items` WHERE `bill_no`='".$_REQUEST['bno']."' AND `status`=1";
			
			$this->Query($query_com);
			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 
		}


		function print_bill(){
		$query_com ="SELECT * FROM `bill_items` WHERE `bill_no`='".$_REQUEST['bno']."' AND `status`=1";
			
			$this->Query($query_com);
			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 
		}

		function delete(){

		}
	
	
	}
