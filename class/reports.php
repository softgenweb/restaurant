<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class reportClass extends DbAccess {
		public $view='';
		public $name='report';
		
			
		function show(){

			$dateFrom = $_REQUEST['from_date'] ? " AND `date_created` LIKE '" . $_REQUEST['from_date'] . "%'" : '';
	        $dateTo   = $_REQUEST['to_date'] ? " AND `date_created` LIKE '" . $_REQUEST['to_date'] . "%'" : '';
	        $bydate   = $dateFrom ? $dateFrom : $dateTo;
			$to = strtotime("+1 day", strtotime($_REQUEST['to_date']));	        
	        $date     = ($dateFrom && $dateTo) ? " AND `date_created`  BETWEEN  '" . $_REQUEST['from_date'] . "%' AND  '" . date("Y-m-d", $to) . "%'" : $bydate;
			$mobile = $_REQUEST['mobile'] ? " AND `mobile` LIKE '%" . $_REQUEST['mobile'] . "%'" : '';
			$bill_no = $_REQUEST['bill_no'] ? " AND `bill_no` LIKE '%" . $_REQUEST['bill_no'] . "%'" : '';

			$payment_mode = $_REQUEST['select_paymode'] ? " AND `payment_mode` ='" . $_REQUEST['select_paymode'] . "'" : '';
				

			if($_REQUEST['search']){
				$uquery = "SELECT * FROM `bill_fare` WHERE 1  $date $mobile $payment_mode $bill_no ORDER BY `id` DESC";
			}else{
				$uquery = "SELECT * FROM `bill_fare` WHERE 1 AND `date_created` LIKE '".date('Y-m-d')."%' ORDER BY `id` DESC";
			}
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row=count($uresults);	
			$_SESSION['country'] = $uquery;
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
				if($_SESSION['utype']=='Employee' && $_SESSION['post']=='2'){
				$query = $uquery;
				}else{
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				}
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 		
		}

		function addnew(){

		}

		function activity(){

			$uquery = "SELECT * FROM `activity_log` WHERE 1 ORDER BY `id` DESC";

			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row=count($uresults);	
			$_SESSION['country'] = $uquery;
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
				
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 				

		}

		function status(){

		}

		function items_wise(){

			$dateFrom = $_REQUEST['from_date'] ? " AND `bill_date` LIKE '" . $_REQUEST['from_date'] . "%'" : '';
	        $dateTo   = $_REQUEST['to_date'] ? " AND `bill_date` LIKE '" . $_REQUEST['to_date'] . "%'" : '';
	        $bydate   = $dateFrom ? $dateFrom : $dateTo;
			$to = strtotime("+1 day", strtotime($_REQUEST['to_date']));	        
	        $date     = ($dateFrom && $dateTo) ? " AND `bill_date`  BETWEEN  '" . $_REQUEST['from_date'] . "%' AND  '" . date("Y-m-d", $to) . "%'" : $bydate;
			// $mobile = $_REQUEST['mobile'] ? " AND `mobile` LIKE '%" . $_REQUEST['mobile'] . "%'" : '';
			// $bill_no = $_REQUEST['bill_no'] ? " AND `bill_no` LIKE '%" . $_REQUEST['bill_no'] . "%'" : '';

			$product_id = $_REQUEST['items'] ? " AND `product_id` ='" . $_REQUEST['items'] . "'" : '';
			if($_REQUEST['search']){
			 	$uquery = "SELECT * FROM `bill_items` WHERE 1 $date $items $product_id ORDER BY `id` DESC";
			}else{
				$uquery = "SELECT * FROM `bill_items` WHERE 1 AND `bill_date` LIKE '".date('Y-m-d')."%' ORDER BY `id` DESC";
			}
			
            $_SESSION['item_wise'] = $uquery;
			
			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row=count($uresults);	
			
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
				
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 	
		}

		function profit_report(){

			$uquery = "SELECT * FROM `product_list` WHERE 1 ORDER BY `product_name` ASC";		

			$this->Query($uquery);
			$uresults = $this->fetchArray();
			$no_of_row=count($uresults);	
			$_SESSION['items'] = $uquery;
			$tdata=count($uresults);

			/* Paging start here */
				$page   = intval($_REQUEST['page']);
				$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
				$adjacents  = intval($_REQUEST['adjacents']);
				$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
				$tpages);//$_GET['tpages'];// 
				$tdata = floor($tdata);
				if($page<=0)  $page  = 1;
				if($adjacents<=0) $tdata?($adjacents = 4):0;
				$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
				
				$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
				
			$this->Query($query);
			$results = $this->fetchArray();		
			
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
	
	
	}
