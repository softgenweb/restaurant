<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class staffClass extends DbAccess {
		public $view='';
		public $name='staff';
		
		
		/***************************************************** Country START **********************************************************/
		
		function show(){	
		$uquery ="select * from users where utype='Employee'";
		$this->Query($uquery);
		$uresults = $this->fetchArray();
		$no_of_row=count($uresults);	
		$_SESSION['country'] = $uquery;
		$tdata=count($uresults);

		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
			
		}
			
		function addnew(){				
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM users WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}
		
		function save(){
		
			$name = mysql_real_escape_string(ucwords($_POST['name']));	
			$username = $_POST['username'];	
			$mobile = $_POST['mobile'];	
			$email = $_POST['email'];	
			$post =  $_POST['post_id'];
			$status = '1';
			$utype = 'Employee';
			$date = date('Y-m-d');
			$id  = $_REQUEST['id'];
		
			$users = mysql_num_rows(mysql_query("select * from users where mobile='".$mobile."'"));
			
			if($users == '0') {
			
				if(!$id){
					$query = "INSERT INTO `users`(`name`, `mobile`, `email`, `post`, `date`, `utype`) VALUES ('".$name."','".$mobile."','".$email."','".$post."','".date('Y-m-d')."','".$utype."')";



					$this->Query($query);
					$this->Execute();		
					$last_id = mysql_insert_id();			
					
					
					$update= "UPDATE `users` SET  `username`='EMP00".$last_id."', `password`='123456' WHERE id='".$last_id."'";						
					$this->Query($update);
					$this->Execute();
			/*===================Activity Log====================*/
			$activity = "Add New Staff (".$name."/".$mobile.")  by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
		
					$_SESSION['alertmessage'] = ADDNEWRECORD; 
					$_SESSION['errorclass'] = SUCCESSCLASS;
					header("location:index.php?control=staff&task=addnew");
				      }
				}
				else {
					$_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;
					header("location:index.php?control=staff&task=addnew");
				} 
				
				if($id){
						$users = mysql_num_rows(mysql_query("select * from users where mobile='".$mobile."' and id!='".$id."'"));
		                if($users == '0') {
					         $update="UPDATE `users` SET `name`='".$name."',  `mobile`='".$mobile."', `post`='".$post."' where id='".$id."'";		
					         $this->Query($update);
					        $this->Execute();
					//$_SESSION['msg'] = '1';
			/*===================Activity Log====================*/
			$activity = "Update detail of Staff (".$name."/".$mobile.")  by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/					        
					   $_SESSION['alertmessage'] = UPDATERECORD; 
					   $_SESSION['errorclass'] = SUCCESSCLASS;
					  header("location:index.php?control=staff&task=show");
						}
					else {
				      $_SESSION['alertmessage'] = DUPLICATE; 
				      $_SESSION['errorclass'] = ERRORCLASS;
				      header("location:index.php?control=staff&task=addnew");
				    } 
					
				}
			
		/*	 $_SESSION['alertmessage'] = DUPLICATE; 
			   $_SESSION['errorclass'] = ERRORCLASS;
	     	header("location:index.php?control=staff&task=addnew");*/
		}
		
		function emp_post(){
			$query_com ="SELECT * FROM `hrm_post` WHERE `id`!=1";
			$this->Query($query_com);

			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 
		}

		function addnew_post(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM `hrm_post` WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}			
		}	

		function save_post(){
			$name = strtoupper($_REQUEST['name']);
			$id = $_REQUEST['id'];

			if(!$id){
				$query =  mysql_query("INSERT INTO `hrm_post`(`post_name`, `date_created`) VALUES ('".$name."','".date('Y-m-d H:i:s')."')");
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
							/*===================Activity Log====================*/
			$activity = "Add new Post (".$name.")  by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/	
			}else{
				$query = mysql_query("UPDATE `hrm_post` SET `post_name`='".$name."' WHERE `id`='".$id."'");
			/*===================Activity Log====================*/
			$activity = "Update Post (".$name.")  by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/	
				$_SESSION['alertmessage'] = UPDATERECORD;
			}
				mysql_query($query);
				$_SESSION['errorclass'] = SUCCESSCLASS;
				
				header("location:index.php?control=staff&task=emp_post");
		}	
		
		function status(){
		$query="update users set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		/*===================Activity Log====================*/
		$activity = "Change Status of Employee (".$_REQUEST['id'].") by ".$_SESSION['username'];

		$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
		/*===================================================*/
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=staff&task=show");
		}			
		
		function post_status(){
		$query="update hrm_post set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		/*===================Activity Log====================*/
		$activity = "Change Status of Post (".$_REQUEST['id'].") by ".$_SESSION['username'];

		$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
		/*===================================================*/
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=staff&task=emp_post");
		}		
		
		function delete(){
		
		$query="DELETE FROM users WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';			
		$_SESSION['alertmessage'] = DELETE; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		
		header("location:index.php?control=staff&task=show");
		}
	
	
	}
