<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class paymentClass extends DbAccess {
		public $view='';
		public $name='payment';
		
			
		function pay_mode(){
			$query = "SELECT * FROM `payment_mode` WHERE 1 ORDER BY `mode` ASC";
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/".$this->task.".php");			
		}

		function addnew(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM payment_mode WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save(){
			$mode = strtoupper($_REQUEST['mode']);		
			$id = $_REQUEST['id'];

			if(!$id){
				$query = "INSERT INTO `payment_mode`(`mode`, `date_created`) VALUES ('".$mode."', '".date('Y-m-d H:i:s')."')";
				$_SESSION['alertmessage'] = ADDNEWRECORD; 


			/*===================Activity Log====================*/
			$activity = "Add Payment Mode (".$mode.") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/

			}else{
				$query = "UPDATE `payment_mode` SET `mode`='".$mode."' WHERE `id`='".$id."'";
				$_SESSION['alertmessage'] = UPDATERECORD; 
			/*===================Activity Log====================*/
			$activity = "Update Payment Mode (".$mode.") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
			}

			mysql_query($query);
			$_SESSION['errorclass'] = SUCCESSCLASS;
			
			header("location:index.php?control=payment&task=pay_mode");
		}

		function show_tax(){

			$query_com ="SELECT * FROM `bill_tax` WHERE 1 ORDER BY ABS(`percent`) ASC ";
			$this->Query($query_com);

			$results = $this->fetchArray();
		    require_once("views/".$this->name."/".$this->task.".php"); 

		}

		function addnew_tax(){

			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM bill_tax WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save_tax(){

			$tax = strtoupper($_REQUEST['tax']);	
			$percent = $_REQUEST['percent'];	
			// $amount = $_REQUEST['amount'];	
			$remark = $_REQUEST['remark'];

			$id = $_REQUEST['id'];

			if(!$id){
			$query = mysql_query("INSERT INTO `bill_tax`(`tax`, `percent`, `remark`, `created_by`, `date_created`) VALUES ('".$tax."', '".$percent."', '".$remark."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");

			$_SESSION['alertmessage'] = ADDNEWRECORD;

			/*===================Activity Log====================*/
			$activity = "Add Tax (".$percent."%) by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/

			}else{
			$query = mysql_query("UPDATE `bill_tax` SET `tax`='".$tax."', `percent`='".$percent."', `remark`='".$remark."' WHERE `id`='".$id."'");
			
			$_SESSION['alertmessage'] = UPDATERECORD;
			/*===================Activity Log====================*/
			$activity = "Update Tax (".$percent."%) by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/			
			}
			// mysql_query($query);
			$_SESSION['errorclass'] = SUCCESSCLASS;

			header("location:index.php?control=payment&task=show_tax");
		}

		function show_discount(){
			$query_com ="SELECT * FROM `bill_discount` WHERE 1 ORDER BY ABS(`percent`) ASC ";
			$this->Query($query_com);

			$results = $this->fetchArray();
		    require_once("views/".$this->name."/".$this->task.".php"); 

		}

		function add_discount(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM bill_discount WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}

		function save_discount(){

	
			$percent = $_REQUEST['percent'];	
			$remark = $_REQUEST['remark'];

			$id = $_REQUEST['id'];

			if(!$id){
			$query = "INSERT INTO `bill_discount`(`percent`, `remark`, `date_created`) VALUES ('".$percent."', '".$remark."', '".date('Y-m-d H:i:s')."')";

			$_SESSION['alertmessage'] = ADDNEWRECORD;
			/*===================Activity Log====================*/
			$activity = "Add Discount (".$percent."%) by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
			}else{
			$query = "UPDATE `bill_discount` SET `percent`='".$percent."', `remark`='".$remark."' WHERE `id`='".$id."'";
			
			$_SESSION['alertmessage'] = UPDATERECORD;
			/*===================Activity Log====================*/
			$activity = "Update Discount (".$percent."%) by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/			
			}
			mysql_query($query);
			$_SESSION['errorclass'] = SUCCESSCLASS;

			header("location:index.php?control=payment&task=show_discount");
		}



		function status(){
	
		$query="UPDATE `payment_mode` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		/*===================Activity Log====================*/
		$activity = "Change Status of payment mode (".$_REQUEST['id'].")  by ".$_SESSION['username'];

		$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
	//	echo "INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')";exit;
		/*===================================================*/
		$this->task="show";
		$this->view ='show';
		//$this->show();	
		header("location:index.php?control=payment&task=pay_mode");

		}

		function tax_status(){
	
		$query="UPDATE `bill_tax` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		/*===================Activity Log====================*/
		$activity = "Change Status of Tax (".$_REQUEST['id'].") by ".$_SESSION['username'];

		$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
		/*===================================================*/
		$this->task="show";
		$this->view ='show';
		//$this->show();	
		header("location:index.php?control=payment&task=show_tax");

		}

		function disc_status(){
	
		$query="UPDATE `bill_discount` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		/*===================Activity Log====================*/
		$activity = "Change status of Discount (".$_REQUEST['id'].") by ".$_SESSION['username'];

		$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
		/*===================================================*/
		$this->task="show";
		$this->view ='show';
		//$this->show();	
		header("location:index.php?control=payment&task=show_discount");

		}

		function delete(){

		}
	
	
	}
