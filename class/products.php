<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class productClass extends DbAccess {
		public $view='';
		public $name='product';
		
			
		function show(){
			$query = "SELECT * FROM `product_list` WHERE 1 ORDER BY `product_name` ASC";
			
			$this->Query($query);
			$results = $this->fetchArray();		
			$_SESSION['items'] = $query;
			require_once("views/".$this->name."/".$this->task.".php");			
		}		
			
		function product_category(){
			$query = "SELECT * FROM `product_category` WHERE 1 ORDER BY `name` ASC";
			
			$this->Query($query);
			$results = $this->fetchArray();		
			$_SESSION['items'] = $query;
			require_once("views/".$this->name."/".$this->task.".php");			
		}

		function addnew(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM product_list WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}

		function addnew_category(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM `product_category` WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}


		function save_category(){
			$name = $_REQUEST['name'];
			$remark = $_REQUEST['remark'];

			$id = $_REQUEST['id'];

			if(!$id){
				$sql = mysql_query("INSERT INTO `product_category`(`name`, `remark`, `date_created`) VALUES ('".$name."','".$remark."', '".date('Y-m-d H:i:s')."')");

			/*===================Activity Log====================*/
			$activity = "Add Item Category (".$name.") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/	
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;						
			}else{
				$sql = mysql_query("UPDATE `product_category` SET `name`='".$name."', `remark`='".$remark."' WHERE `id`='".$id."'");
			/*===================Activity Log====================*/
			$activity = "Update Item Category (".$name.") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;					
			}
				header("location:index.php?control=product&task=product_category");
		}

		function save(){
			$product_name = mysql_real_escape_string(strtoupper($_REQUEST['product_name']));			
			$category_id = $_REQUEST['category_id'];			
			$qty = $_REQUEST['qty'];
			$price = $_REQUEST['price'];		
			$discount_type = $_REQUEST['disc_type'];
			$discount = $_REQUEST['discount'];
			$remark = mysql_real_escape_string($_REQUEST['remark']);
				

			$id = $_REQUEST['id'];

			if(!$id){
				$chk = mysql_fetch_array(mysql_query("SELECT `id` FROM `product_list` WHERE `product_name` LIKE '%".$product_name."%'"));
				if(!$chk){
				 $query = mysql_query("INSERT INTO `product_list`(`product_name`, `category_id`, `qty`, `price`, `discount_type`, `discount`, `remark`, `date_created`, `created_by`, `date_modify`) VALUES ('".$product_name."', '".$category_id."', '".$qty."', '".$price."', '".$discount_type."', '".$discount."', '".$remark."','".date('Y-m-d H:i:s')."','".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");

			/*===================Activity Log====================*/
			$activity = "Add Item (".$product_name.") of Price ".$price."/- With Discount ".$discount."/- by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
			}else{
				$_SESSION['alertmessage'] = DUPLICATE;
				$_SESSION['errorclass'] = ERRORCLASS;
			}

			}else{
				$query = mysql_query("UPDATE `product_list` SET `product_name`='".$product_name."', `category_id`='".$category_id."', `qty`='".$qty."', `price`='".$price."', `discount_type`='".$discount_type."', `discount`='".$discount."', `remark`='".$remark."' WHERE `id`='".$id."'");
			/*===================Activity Log====================*/
			$activity = "Update Item (".$product_name.") of Price ".$price."/- With Discount ".$discount."/- by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
			}

				// mysql_query($query);
				header("location:index.php?control=product&task=show");
		}


		function category_status(){
	
			$query="UPDATE `product_category` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();

			/*===================Activity Log====================*/
			$activity = "Change Status of Product Category (".$_REQUEST['id'].") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
			$this->task="show";
			$this->view ='show';
			//$this->show();	
			header("location:index.php?control=product&task=product_category");

		}

		function status(){
		
			$query="UPDATE `product_list` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();

			/*===================Activity Log====================*/
			$activity = "Change Status of Product (".$_REQUEST['id'].") by ".$_SESSION['username'];

			$add = mysql_query("INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/
			$this->task="show";
			$this->view ='show';
			//$this->show();	
			header("location:index.php?control=product&task=show");

		}


		function product_sales(){
			$pid = $_REQUEST['id'];


			$dateFrom = $_REQUEST['from_date'] ? " AND `bill_date` LIKE '" . $_REQUEST['from_date'] . "%'" : '';
			$dateTo   = $_REQUEST['to_date'] ? " AND `bill_date` LIKE '" . $_REQUEST['to_date'] . "%'" : '';
			$bydate   = $dateFrom ? $dateFrom : $dateTo;
			$to = strtotime("+1 day", strtotime($_REQUEST['to_date']));	        
			$date     = ($dateFrom && $dateTo) ? " AND `bill_date`  BETWEEN  '" . $_REQUEST['from_date'] . "%' AND  '" . date("Y-m-d", $to) . "%'" : $bydate;

			$query = "SELECT * FROM `bill_items` WHERE `product_id`='".$pid."' $date ORDER BY `id` DESC";
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/".$this->task.".php");	
		}
	
	
	}
