

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_ip` varchar(200) NOT NULL,
  `activity` varchar(220) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `bill_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `percent` varchar(100) NOT NULL,
  `remark` varchar(225) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='bill discounts';

INSERT INTO bill_discount VALUES("1","0","","2018-12-19 11:13:01","1");
INSERT INTO bill_discount VALUES("2","6","","2018-12-19 11:13:42","1");
INSERT INTO bill_discount VALUES("3","12","","2018-12-19 11:13:53","1");
INSERT INTO bill_discount VALUES("4","15","","2018-12-19 11:13:53","1");
INSERT INTO bill_discount VALUES("5","Other Amount","only for other amount","2018-12-19 11:14:11","1");
INSERT INTO bill_discount VALUES("6","5","","2018-12-22 12:30:19","1");





CREATE TABLE `bill_fare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_no` varchar(220) NOT NULL,
  `token_no` int(11) NOT NULL,
  `customer_name` varchar(220) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `total_item` int(11) NOT NULL COMMENT 'number of total product/particuler',
  `total_qty` varchar(220) NOT NULL,
  `tax_percent` varchar(100) NOT NULL,
  `total_tax` varchar(200) NOT NULL,
  `disc_percent` varchar(100) NOT NULL,
  `total_discount` varchar(220) NOT NULL,
  `total_amount` varchar(220) NOT NULL,
  `grand_total` varchar(220) NOT NULL,
  `financial_year` varchar(220) NOT NULL,
  `payment_mode` int(11) NOT NULL,
  `card_number` bigint(100) NOT NULL COMMENT 'card last 4 number',
  `date_created` datetime NOT NULL,
  `date_modify` varchar(220) NOT NULL,
  `emp_id` bigint(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `bill_items` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT,
  `bill_id` bigint(100) NOT NULL,
  `bill_no` varchar(220) NOT NULL,
  `cust_mobile` varchar(100) NOT NULL,
  `product_id` int(100) NOT NULL,
  `product_name` varchar(225) NOT NULL,
  `qty` int(220) NOT NULL,
  `rate` varchar(100) NOT NULL,
  `discount` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `remark` varchar(220) NOT NULL,
  `financial_year` varchar(100) NOT NULL,
  `bill_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `bill_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax` varchar(100) NOT NULL,
  `percent` varchar(100) NOT NULL DEFAULT 'other',
  `remark` varchar(200) NOT NULL,
  `created_by` bigint(100) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO bill_tax VALUES("1","GST","5","","1","2018-12-15 19:03:36","1");
INSERT INTO bill_tax VALUES("2","GST","0","","1","2018-12-15 19:03:36","1");
INSERT INTO bill_tax VALUES("3","GST","12","","1","2018-12-15 19:03:36","1");
INSERT INTO bill_tax VALUES("4","OTHER AMOUNT","Other Amount","","1","2018-12-15 19:07:07","1");
INSERT INTO bill_tax VALUES("5","GST","18","","1","2018-12-22 12:20:39","1");





CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `image` varchar(220) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO company VALUES("1","Kavyashree Foodworks","gaurav.s@softgentechnologies.com","0544623434","8299431080","LDA Colony Near Power House Chauraha , Lucknow","37ADAPM1724A2Z5","QWERT1234A","1/1logo.gif","2018-12-15 16:12:53","2018-12-22 12:11:42","1");





CREATE TABLE `confic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `confic_type_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `control` varchar(200) DEFAULT 'text',
  PRIMARY KEY (`id`,`confic_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT INTO confic VALUES("1","1","paging","100","Page should be #5, 10, 20, 50, 100, 1000","select");
INSERT INTO confic VALUES("3","1","default_mail","narai1987@gmail.com","Default mail to communicate with user","text");
INSERT INTO confic VALUES("4","1","max_count","100","","");
INSERT INTO confic VALUES("5","2","language","1","","");
INSERT INTO confic VALUES("6","1","service_tax","8","","");
INSERT INTO confic VALUES("10","1","join_point","100","","");
INSERT INTO confic VALUES("11","1","rating_point","10","","");
INSERT INTO confic VALUES("12","1","get_point_on_purchase_product_per_1000_USD","100","","");
INSERT INTO confic VALUES("13","1","login_point","5","","");
INSERT INTO confic VALUES("14","1","price_per_100_point","2","","");
INSERT INTO confic VALUES("15","1","admin_gift_point","50","","");
INSERT INTO confic VALUES("16","1","minimum_used_point","500","","");
INSERT INTO confic VALUES("17","1","point_per_beverage","5","","");
INSERT INTO confic VALUES("18","1","point_per_eqipment","5","","");
INSERT INTO confic VALUES("19","1","point_per_food","5","","");
INSERT INTO confic VALUES("20","1","point_per_cabin","10","","");
INSERT INTO confic VALUES("21","1","trip_low_price_range","5000","","text");
INSERT INTO confic VALUES("22","1","trip_high_price_range","25000","","text");





CREATE TABLE `customer_details` (
  `id` bigint(100) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(100) NOT NULL,
  `name` varchar(220) NOT NULL,
  `mobile` varchar(220) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL COMMENT 'date_of_birth/ anniversery',
  `address` text NOT NULL,
  `date_created` datetime NOT NULL,
  `emp_id` bigint(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `hrm_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_name` varchar(220) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO hrm_post VALUES("1","ADMIN","2018-12-17 11:17:41","1");
INSERT INTO hrm_post VALUES("2","MANAGER","2018-12-17 11:46:51","1");
INSERT INTO hrm_post VALUES("3","EMPLOYEE","2018-12-17 11:47:06","1");





CREATE TABLE `payment_mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mode` varchar(200) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO payment_mode VALUES("1","DEBIT CARD","2018-12-15 16:38:30","1");
INSERT INTO payment_mode VALUES("2","CASH","2018-12-15 18:43:51","1");
INSERT INTO payment_mode VALUES("3","CREDIT CARD","2018-12-15 18:44:17","1");
INSERT INTO payment_mode VALUES("4","PAYTM","2018-12-22 12:13:25","1");
INSERT INTO payment_mode VALUES("5","GIFT CARD","2018-12-27 13:34:39","1");
INSERT INTO payment_mode VALUES("6","ZOMATO","2018-12-27 13:35:26","1");
INSERT INTO payment_mode VALUES("7","FOODPANDA","2018-12-27 13:35:46","1");
INSERT INTO payment_mode VALUES("8","GOOGLE PAY","2018-12-27 13:36:09","1");
INSERT INTO payment_mode VALUES("9","PHONEPE","2018-12-27 13:36:24","1");
INSERT INTO payment_mode VALUES("10","BHIM UPI","2018-12-27 13:36:43","1");
INSERT INTO payment_mode VALUES("11","COD","2018-12-27 13:37:07","1");
INSERT INTO payment_mode VALUES("12","SWIGGY","2018-12-27 14:48:33","1");





CREATE TABLE `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) NOT NULL,
  `remark` varchar(225) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO product_category VALUES("1","Combos","","2019-01-21 13:07:03","1");
INSERT INTO product_category VALUES("2","Dosa","","2019-01-21 13:27:30","1");
INSERT INTO product_category VALUES("3","Idli","","2019-01-21 13:27:41","1");
INSERT INTO product_category VALUES("4","Gravy & Choice of Rice","","2019-01-21 13:27:41","1");
INSERT INTO product_category VALUES("5","Snacks","","2019-01-21 13:27:41","1");
INSERT INTO product_category VALUES("6","Beverages","","2019-01-21 13:27:41","1");
INSERT INTO product_category VALUES("7","Ice Cream & Sweets","","2019-01-21 13:27:41","1");





CREATE TABLE `product_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(220) NOT NULL,
  `category_id` int(11) NOT NULL,
  `qty` varchar(100) NOT NULL DEFAULT '1',
  `price` varchar(100) NOT NULL,
  `discount_type` varchar(255) NOT NULL COMMENT 'Other Means manual Amount number means %',
  `discount` varchar(100) NOT NULL,
  `remark` varchar(220) NOT NULL,
  `date_created` datetime NOT NULL,
  `created_by` bigint(100) NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

INSERT INTO product_list VALUES("1","Regular (Plain Dosa)","2","1","100","0","0","Plain Dosa","2019-01-23 12:00:20","1","2019-01-23 12:00:20","1");
INSERT INTO product_list VALUES("2","Regular (Masala Dosa)","2","1","120","0","0","Masala Dosa","2019-01-23 12:00:21","1","2019-01-23 12:00:21","1");
INSERT INTO product_list VALUES("3","Regular (Onion Dosa)","2","1","110","0","0","Onion Dosa","2019-01-23 12:00:22","1","2019-01-23 12:00:22","1");
INSERT INTO product_list VALUES("4","Regular (Onion Masala Dosa)","2","1","120","0","0","Onion Masala Dosa","2019-01-23 12:00:23","1","2019-01-23 12:00:23","1");
INSERT INTO product_list VALUES("5","Regular (Aloo Veg Masala MIX Dosa)","2","1","140","0","0","Aloo Veg Masala MIX Dosa","2019-01-23 12:00:24","1","2019-01-23 12:00:24","1");
INSERT INTO product_list VALUES("6","Regular (Mysore Masala Dosa)","2","1","140","0","0","Mysore Masala Dosa","2019-01-23 12:00:25","1","2019-01-23 12:00:25","1");
INSERT INTO product_list VALUES("7","Regular (Mysore Paneer Dosa)","2","1","180","0","0","Mysore Paneer Dosa","2019-01-23 12:00:26","1","2019-01-23 12:00:26","1");
INSERT INTO product_list VALUES("8","Regular (Paneer Dosa)","2","1","180","0","0","Paneer Dosa","2019-01-23 12:00:27","1","2019-01-23 12:00:27","1");
INSERT INTO product_list VALUES("9","Regular (Paneer Masala Mix Dosa)","2","1","180","0","0","Paneer Masala Mix Dosa","2019-01-23 12:00:28","1","2019-01-23 12:00:28","1");
INSERT INTO product_list VALUES("10","Regular (Set Dosa Dosa)","2","1","140","0","0","Set Dosa Dosa","2019-01-23 12:00:29","1","2019-01-23 12:00:29","1");
INSERT INTO product_list VALUES("11","Green (Palak Plain Dosa)","2","1","120","0","0","Palak Plain Dosa","2019-01-23 12:00:30","1","2019-01-23 12:00:30","1");
INSERT INTO product_list VALUES("12","Green (Palak Masala Dosa)","2","1","140","0","0","Palak Masala Dosa","2019-01-23 12:00:31","1","2019-01-23 12:00:31","1");
INSERT INTO product_list VALUES("13","Green (Green Peas Dosa)","2","1","180","0","0","Green Peas Dosa","2019-01-23 12:00:32","1","2019-01-23 12:00:32","1");
INSERT INTO product_list VALUES("14","Green (Green Capsicum Dosa)","2","1","140","0","0","Green Capsicum Dosa","2019-01-23 12:00:33","1","2019-01-23 12:00:33","1");
INSERT INTO product_list VALUES("15","Green (Green Coriander Dosa)","2","1","140","0","0","Green Coriander Dosa","2019-01-23 12:00:34","1","2019-01-23 12:00:34","1");
INSERT INTO product_list VALUES("16","Chineese (Chineese Schezwan Dosa)","2","1","140","0","0","Chineese Schezwan Dosa","2019-01-23 12:00:35","1","2019-01-23 12:00:35","1");
INSERT INTO product_list VALUES("17","Chineese (Chineese Veg Manchurian Dosa)","2","1","180","0","0","Chineese Veg Manchurian Dosa","2019-01-23 12:00:36","1","2019-01-23 12:00:36","1");
INSERT INTO product_list VALUES("18","Chineese (Chineese Spring Wrap Dosa)","2","1","180","0","0","Chineese Spring Wrap Dosa","2019-01-23 12:00:37","1","2019-01-23 12:00:37","1");
INSERT INTO product_list VALUES("19","Chineese (Chineese Chilli Paneer Dosa)","2","1","180","0","0","Chineese Chilli Paneer Dosa","2019-01-23 12:00:38","1","2019-01-23 12:00:38","1");
INSERT INTO product_list VALUES("20","Special (Peanut Plain Dosa)","2","1","140","0","0","Peanut Plain Dosa","2019-01-23 12:00:39","1","2019-01-23 12:00:39","1");
INSERT INTO product_list VALUES("21","Special (Pav Bhaji Masala Dosa)","2","1","180","0","0","Pav Bhaji Masala Dosa","2019-01-23 12:00:40","1","2019-01-23 12:00:40","1");
INSERT INTO product_list VALUES("22","Special (Chana Dal fry Dosa)","2","1","180","0","0","Chana Dal fry Dosa","2019-01-23 12:00:41","1","2019-01-23 12:00:41","1");
INSERT INTO product_list VALUES("23","Special (Corn fry Dosa)","2","1","180","0","0","Corn fry Dosa","2019-01-23 12:00:42","1","2019-01-23 12:00:42","1");
INSERT INTO product_list VALUES("24","Special (Mushroom fry Dosa)","2","1","180","0","0","Mushroom fry Dosa","2019-01-23 12:00:43","1","2019-01-23 12:00:43","1");
INSERT INTO product_list VALUES("25","Special (Garlic roasted Dosa)","2","1","140","0","0","Garlic roasted Dosa","2019-01-23 12:00:44","1","2019-01-23 12:00:44","1");
INSERT INTO product_list VALUES("26","Special (Twin Tower Dosa)","2","1","180","0","0","Twin Tower Dosa","2019-01-23 12:00:45","1","2019-01-23 12:00:45","1");
INSERT INTO product_list VALUES("27","Fusion (Fusion Mix Veg Dosa)","2","1","220","0","0","Fusion Mix Veg Dosa","2019-01-23 12:00:46","1","2019-01-23 12:00:46","1");
INSERT INTO product_list VALUES("28","Fusion (Fusion Mushroom Dosa)","2","1","220","0","0","Fusion Mushroom Dosa","2019-01-23 12:00:47","1","2019-01-23 12:00:47","1");
INSERT INTO product_list VALUES("29","Fusion (Fusin Corn Dosa)","2","1","220","0","0","Fusin Corn Dosa","2019-01-23 12:00:48","1","2019-01-23 12:00:48","1");
INSERT INTO product_list VALUES("30","Regular (Rawa Plain Dosa)","2","1","100","0","0","Rawa Plain Dosa","2019-01-23 12:00:49","1","2019-01-23 12:00:49","1");
INSERT INTO product_list VALUES("31","Regular (Rawa Masala Dosa)","2","1","120","0","0","Rawa Masala Dosa","2019-01-23 12:00:50","1","2019-01-23 12:00:50","1");
INSERT INTO product_list VALUES("32","Regular (Rawa Onion Dosa)","2","1","110","0","0","Rawa Onion Dosa","2019-01-23 12:00:51","1","2019-01-23 12:00:51","1");
INSERT INTO product_list VALUES("33","Regular (Rawa Onion Masala Dosa)","2","1","120","0","0","Rawa Onion Masala Dosa","2019-01-23 12:00:52","1","2019-01-23 12:00:52","1");
INSERT INTO product_list VALUES("34","Regular (Rawa Aloo Veg Masala MIX Dosa)","2","1","140","0","0","Rawa Aloo Veg Masala MIX Dosa","2019-01-23 12:00:53","1","2019-01-23 12:00:53","1");
INSERT INTO product_list VALUES("35","Green (Rawa Palak Plain Dosa)","2","1","120","0","0","Rawa Palak Plain Dosa","2019-01-23 12:00:54","1","2019-01-23 12:00:54","1");
INSERT INTO product_list VALUES("36","Green (Rawa Palak Masala Dosa)","2","1","140","0","0","Rawa Palak Masala Dosa","2019-01-23 12:00:55","1","2019-01-23 12:00:55","1");
INSERT INTO product_list VALUES("37","Green (Rawa Green Peas Dosa)","2","1","180","0","0","Rawa Green Peas Dosa","2019-01-23 12:00:56","1","2019-01-23 12:00:56","1");
INSERT INTO product_list VALUES("38","Green (Rawa Capsicum Dosa)","2","1","140","0","0","Rawa Capsicum Dosa","2019-01-23 12:00:57","1","2019-01-23 12:00:57","1");
INSERT INTO product_list VALUES("39","Green (Rawa Coriander Dosa)","2","1","140","0","0","Rawa Coriander Dosa","2019-01-23 12:00:58","1","2019-01-23 12:00:58","1");
INSERT INTO product_list VALUES("40","Special (Rawa Peanut Plain Dosa)","2","1","140","0","0","Rawa Peanut Plain Dosa","2019-01-23 12:00:59","1","2019-01-23 12:00:59","1");
INSERT INTO product_list VALUES("41","Special (Rawa Chana Dal fry Dosa)","2","1","180","0","0","Rawa Chana Dal fry Dosa","2019-01-23 12:00:59","1","2019-01-23 12:00:60","1");
INSERT INTO product_list VALUES("42","Special (Rawa Corn fry Dosa)","2","1","180","0","0","Rawa Corn fry Dosa","2019-01-23 12:00:59","1","2019-01-23 12:00:61","1");
INSERT INTO product_list VALUES("43","Special (Rawa Mushroom fry Dosa)","2","1","180","0","0","Rawa Mushroom fry Dosa","2019-01-23 12:00:59","1","2019-01-23 12:00:62","1");
INSERT INTO product_list VALUES("44","Special (Rawa Garlic roasted Dosa)","2","1","140","0","0","Rawa Garlic roasted Dosa","2019-01-23 12:00:59","1","2019-01-23 12:00:63","1");
INSERT INTO product_list VALUES("45","Fusion (Rawa Mix Veg Dosa)","2","1","220","0","0","Rawa Mix Veg Dosa","2019-01-23 12:00:59","1","2019-01-23 12:00:64","1");
INSERT INTO product_list VALUES("46","Combo1 (Rice Idli or Vada,Plain Masala Dosa)","1","1","150","0","0","Rice Idli or Vada,Plain Masala Dosa","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("47","Combo2 (Shahi Chole Bhatura with Butter milk)","1","1","160","0","0","Shahi Chole Bhatura with Butter milk","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("48","Combo3 (Shahi Cholley kulcha with Butter milk)","1","1","140","0","0","Shahi Cholley kulcha with Butter milk","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("49","Combo4 (Kurkure Chaat with Tea/Normal Coffee)","1","1","100","0","0","Kurkure Chaat with Tea/Normal Coffee","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("50","Combo5 (Basket Chaat with Tea/Normal Coffee)","1","1","110","0","0","Basket Chaat with Tea/Normal Coffee","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("51","Combo6 (Sarso Ka Saag, Makke ki Roti & Gud)","1","1","180","0","0","Sarso Ka Saag, Makke ki Roti & Gud","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("52","Combo7 (Ghar Ki Thali)","1","1","150","0","0","Ghar Ki Thali","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("53","Combo8 (Special Thali)","1","1","220","0","0","Special Thali","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("54","Combo9 (Punjabi Thali)","1","1","220","0","0","Punjabi Thali","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("55","Combo10 (Rajasthani Thali)","1","1","220","0","0","Rajasthani Thali","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("56","Combo11 (Rice Idli or Vada,Any Special Dosa)","1","1","220","0","0","Rice Idli or Vada,Any Special Dosa","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("57","Combo12 (Green Thali)","1","1","250","0","0","Green Thali","2019-01-23 12:00:59","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("58","Snacks (Chole Bhature)","5","1","119","0","0","Bathura, Chole garnished with Coriander and Garlic, Beetroot Onion, Green Chilli Masala Stuffed","2019-01-21 13:07:03","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("59","Snacks (Shahi Chole Bhature)","5","1","139","0","0","Paneer Bathura, Chole with Malai Kofta garnished with Coriander and Garlic, Beetroot Onion, Green Chilli Masala Stuffed","2019-01-21 13:07:04","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("60","Snacks (Cholley kulcha)","5","1","109","0","0","kulcha, Chole garnished with Coriander and Garlic, Beetroot Onion, Green Chilli Masala Stuffed","2019-01-21 13:07:05","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("61","Snacks (Shahi Cholley kulcha)","5","1","119","0","0","kulcha, Chole with Malai Kofta garnished with Coriander and Garlic, Beetroot Onion, Green Chilli Masala Stuffed","2019-01-21 13:07:06","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("62","Snacks (Dahi Vada)","5","1","79","0","0","Dahi Vada","2019-01-21 13:07:07","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("63","Snacks (Dahi Bhalla)","5","1","79","0","0","Dahi Bhalla","2019-01-21 13:07:08","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("64","Snacks (Kurkure Chaat)","5","1","80","0","0","Kurkure Chaat","2019-01-21 13:07:09","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("65","Snacks (Basket Chaat)","5","1","90","0","0","Basket Chaat","2019-01-21 13:07:10","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("66","Snacks (Pav (1Pcs))","5","1","20","0","0","Pav (1Pcs)","2019-01-21 13:07:11","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("67","Snacks (Special Pav Bhaji (2Pcs))","5","1","110","0","0","Special Pav Bhaji (2Pcs)","2019-01-21 13:07:12","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("68","South Indian (Vegetables Oat Chila)","5","1","99","0","0","Vegetables Oat Chila","2019-01-21 13:07:13","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("69","Italian (fv special Cheese Veg Tikki)","5","1","99","0","0","fv special Cheese Veg Tikki","2019-01-21 13:07:14","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("70","Italian (fv special Arrabbiata Veg (In Red Sauce))","5","1","170","0","0","fv special Arrabbiata Veg (In Red Sauce)","2019-01-21 13:07:15","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("71","Italian (fv special Alfredo Veg. (In White Sauce))","5","1","170","0","0","fv special Alfredo Veg. (In White Sauce)","2019-01-21 13:07:16","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("72","Italian (fv special Veggie Parma Rosa Pasta Bowl (Mix of White and Red Sauce))","5","1","190","0","0","fv special Veggie Parma Rosa Pasta Bowl (Mix of White and Red Sauce)","2019-01-21 13:07:17","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("73","Italian (Stuffed Mushroom)","5","1","99","0","0","Stuffed Mushroom","2019-01-21 13:07:18","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("74","Chineese (Chilli Potato)","5","1","90","0","0","Chilli Potato","2019-01-21 13:07:19","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("75","Chineese (Honey Chilli Potato)","5","1","110","0","0","Honey Chilli Potato","2019-01-21 13:07:20","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("76","Chineese (Chilli Paneer)","5","1","170","0","0","Chilli Paneer","2019-01-21 13:07:21","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("77","Chineese (Chilli Mushroom)","5","1","180","0","0","Chilli Mushroom","2019-01-21 13:07:22","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("78","Chineese (Veg Hakka Noodles)","5","1","140","0","0","Veg Hakka Noodles","2019-01-21 13:07:23","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("79","Chineese (Chilli Garlic Noodles)","5","1","160","0","0","Chilli Garlic Noodles","2019-01-21 13:07:24","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("80","Chineese (Singapore Noodles)","5","1","180","0","0","Singapore Noodles","2019-01-21 13:07:25","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("81","Chineese (Schezwan Noodles)","5","1","140","0","0","Schezwan Noodles","2019-01-21 13:07:26","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("82","Chineese (Kind Pao Veg Noodles)","5","1","199","0","0","Kind Pao Veg Noodles","2019-01-21 13:07:27","1","23-01-2019 12:00","1");
INSERT INTO product_list VALUES("83","Rice Idli Sambar (2 Pcs)","3","1","90","0","0","Dip idli in Sambar","2019-01-23 12:00:20","1","2019-01-23 12:00:20","1");
INSERT INTO product_list VALUES("84","Rice Idli Sambar (4 Pcs)","3","1","145","0","0","Dip idli in Sambar","2019-01-23 12:00:21","1","2019-01-23 12:00:21","1");
INSERT INTO product_list VALUES("85","Sambar Vada","3","1","90","0","0","Idli Vada in Sambar","2019-01-23 12:00:22","1","2019-01-23 12:00:22","1");
INSERT INTO product_list VALUES("86","Dahi Vada","3","1","90","0","0","Idli in Dahi with Imli Chatni, Chaat Masala etc.","2019-01-23 12:00:23","1","2019-01-23 12:00:23","1");
INSERT INTO product_list VALUES("87","Beetroot Idli Sambar","3","1","100","0","0","Stuffed, Stuffed Beetroot in Idli Batter while steam","2019-01-23 12:00:24","1","2019-01-23 12:00:24","1");
INSERT INTO product_list VALUES("88","Paneer Idli Sambar","3","1","120","0","0","Stuffed","2019-01-23 12:00:25","1","2019-01-23 12:00:25","1");
INSERT INTO product_list VALUES("89","Capsicum Idli Sambar","3","1","100","0","0","Stuffed","2019-01-23 12:00:26","1","2019-01-23 12:00:26","1");
INSERT INTO product_list VALUES("90","Green Peas Idli Sambar","3","1","100","0","0","Stuffed","2019-01-23 12:00:27","1","2019-01-23 12:00:27","1");
INSERT INTO product_list VALUES("91","Palak Idli Sambar","3","1","100","0","0","Stuffed","2019-01-23 12:00:28","1","2019-01-23 12:00:28","1");
INSERT INTO product_list VALUES("92","Onion Idli Sambar","3","1","100","0","0","Fried, fry idli with Onion and spread with masala for taste. Serve seperately with Sambar","2019-01-23 12:00:29","1","2019-01-23 12:00:29","1");
INSERT INTO product_list VALUES("93","Garlic Idli Sambar","3","1","100","0","0","Stuffed, Garlic idli should be serve fried","2019-01-23 12:00:30","1","2019-01-23 12:00:30","1");
INSERT INTO product_list VALUES("94","Coriander Idli Sambar","3","1","100","0","0","Stuffed","2019-01-23 12:00:31","1","2019-01-23 12:00:31","1");
INSERT INTO product_list VALUES("95","Schezwan Idli Sambar","3","1","120","0","0","Fried","2019-01-23 12:00:32","1","2019-01-23 12:00:32","1");
INSERT INTO product_list VALUES("96","Peanut Idli Sambar","3","1","120","0","0","Stuffed, Peanut Idli shoul be served fried","2019-01-23 12:00:33","1","2019-01-23 12:00:33","1");
INSERT INTO product_list VALUES("97","Mysore Masala Idli","3","1","120","0","0","Fried, Onion or Garlic idle can mix with Mysore Masala and served","2019-01-23 12:00:34","1","2019-01-23 12:00:34","1");
INSERT INTO product_list VALUES("98","Chana Daal Idli Sambar","3","1","120","0","0","Stuffed, Stuffed Chana Daal with Idli and fry before serve","2019-01-23 12:00:35","1","2019-01-23 12:00:35","1");
INSERT INTO product_list VALUES("99","Corn Idli Sambar","3","1","120","0","0","Stuffed","2019-01-23 12:00:36","1","2019-01-23 12:00:36","1");
INSERT INTO product_list VALUES("100","Idli Manchurian","3","1","149","0","0","Fried","2019-01-23 12:00:37","1","2019-01-23 12:00:37","1");
INSERT INTO product_list VALUES("101","Idli Chat","3","1","149","0","0","Fried","2019-01-23 12:00:38","1","2019-01-23 12:00:38","1");
INSERT INTO product_list VALUES("102","Dal vada","3","1","90","0","0","Fried, chana dal onion coriender ginger and green chilli mess and deep fry in oil searve sambar and coconut chutni","2019-01-23 12:00:39","1","2019-01-23 12:00:39","1");
INSERT INTO product_list VALUES("103","Bisible Bath Papad (250 Gms)","4","1","120","0","0","Bisible Bath Papad (250 Gms)","2019-01-23 12:00:20","1","2019-01-23 12:00:20","1");
INSERT INTO product_list VALUES("104","Bisible Bath (400 Gms)","4","1","180","0","0","Bisible Bath (400 Gms)","2019-01-23 12:00:21","1","2019-01-23 12:00:21","1");
INSERT INTO product_list VALUES("105","Curd Rice (250 Gms)","4","1","120","0","0","Curd Rice (250 Gms)","2019-01-23 12:00:22","1","2019-01-23 12:00:22","1");
INSERT INTO product_list VALUES("106","Curd Rice (400 Gms)","4","1","180","0","0","Curd Rice (400 Gms)","2019-01-23 12:00:23","1","2019-01-23 12:00:23","1");
INSERT INTO product_list VALUES("107","Lemon Rice (250 gms)","4","1","120","0","0","Lemon Rice (250 gms)","2019-01-23 12:00:24","1","2019-01-23 12:00:24","1");
INSERT INTO product_list VALUES("108","Lemon Rice (400 gms)","4","1","180","0","0","Lemon Rice (400 gms)","2019-01-23 12:00:25","1","2019-01-23 12:00:25","1");
INSERT INTO product_list VALUES("109","Rajma Rice with Raita","4","1","150","0","0","Rajma Rice with Raita","2019-01-23 12:00:26","1","2019-01-23 12:00:26","1");
INSERT INTO product_list VALUES("110","Special Cholley Rice with Raita","4","1","150","0","0","Special Cholley Rice with Raita","2019-01-23 12:00:27","1","2019-01-23 12:00:27","1");
INSERT INTO product_list VALUES("111","Shahi Kadi Rice with Raita","4","1","150","0","0","Shahi Kadi Rice with Raita","2019-01-23 12:00:28","1","2019-01-23 12:00:28","1");
INSERT INTO product_list VALUES("112","Paneer Makhni Rice with Raita","4","1","180","0","0","Paneer Makhni Rice with Raita","2019-01-23 12:00:29","1","2019-01-23 12:00:29","1");
INSERT INTO product_list VALUES("113","Cholley (100 Gms)","4","1","70","0","0","Cholley (100 Gms)","2019-01-23 12:00:30","1","2019-01-23 12:00:30","1");
INSERT INTO product_list VALUES("114","Cholley (250 Gms)","4","1","160","0","0","Cholley (250 Gms)","2019-01-23 12:00:31","1","2019-01-23 12:00:31","1");
INSERT INTO product_list VALUES("115","Rajma (100 Gms)","4","1","90","0","0","Rajma (100 Gms)","2019-01-23 12:00:32","1","2019-01-23 12:00:32","1");
INSERT INTO product_list VALUES("116","Rajma (250 Gms)","4","1","180","0","0","Rajma (250 Gms)","2019-01-23 12:00:33","1","2019-01-23 12:00:33","1");
INSERT INTO product_list VALUES("117","Filter Coffee","6","1","50","0","0","Filter Coffee","2019-01-23 12:00:20","1","2019-01-23 12:00:20","1");
INSERT INTO product_list VALUES("118","Coffee Strong (150 ml)","6","1","50","0","0","Coffee Strong (150 ml)","2019-01-23 12:00:21","1","2019-01-23 12:00:21","1");
INSERT INTO product_list VALUES("119","Normal Coffee (150 ml)","6","1","40","0","0","Normal Coffee (150 ml)","2019-01-23 12:00:22","1","2019-01-23 12:00:22","1");
INSERT INTO product_list VALUES("120","Hot Tea (150 ml)","6","1","30","0","0","Hot Tea (150 ml)","2019-01-23 12:00:23","1","2019-01-23 12:00:23","1");
INSERT INTO product_list VALUES("121","Sweet Lassi (250 ml)","6","1","80","0","0","Sweet Lassi (250 ml)","2019-01-23 12:00:24","1","2019-01-23 12:00:24","1");
INSERT INTO product_list VALUES("122","Butter Milk (250 ml)","6","1","60","0","0","Butter Milk (250 ml)","2019-01-23 12:00:25","1","2019-01-23 12:00:25","1");
INSERT INTO product_list VALUES("123","Butterscotch Milkshake (300 ml)","6","1","90","0","0","Butterscotch Milkshake (300 ml)","2019-01-23 12:00:26","1","2019-01-23 12:00:26","1");
INSERT INTO product_list VALUES("124","Strawberry Milkshake (300 ml)","6","1","90","0","0","Strawberry Milkshake (300 ml)","2019-01-23 12:00:27","1","2019-01-23 12:00:27","1");
INSERT INTO product_list VALUES("125","Chocolate Milkshake (300 ml)","6","1","90","0","0","Chocolate Milkshake (300 ml)","2019-01-23 12:00:28","1","2019-01-23 12:00:28","1");
INSERT INTO product_list VALUES("126","Cold Coffee (250 ml)","6","1","90","0","0","Cold Coffee (250 ml)","2019-01-23 12:00:29","1","2019-01-23 12:00:29","1");
INSERT INTO product_list VALUES("127","Ice tea (250ml)","6","1","60","0","0","Ice tea (250ml)","2019-01-23 12:00:30","1","2019-01-23 12:00:30","1");
INSERT INTO product_list VALUES("128","Lemonade (300 ml)","6","1","60","0","0","Lemonade (300 ml)","2019-01-23 12:00:31","1","2019-01-23 12:00:31","1");
INSERT INTO product_list VALUES("129","Gulab Jamun (1 Pcs)","7","1","30","0","0","Gulab Jamun (1 Pcs)","2019-01-23 12:00:20","1","2019-01-23 12:00:20","1");
INSERT INTO product_list VALUES("130","Gulab Jamun (2 Pcs)","7","1","50","0","0","Gulab Jamun (2 Pcs)","2019-01-23 12:00:21","1","2019-01-23 12:00:21","1");
INSERT INTO product_list VALUES("131","Café Special Green Sweet (100 gm)","7","1","70","0","0","Caf? Special Green Sweet (100 gm)","2019-01-23 12:00:22","1","2019-01-23 12:00:22","1");
INSERT INTO product_list VALUES("132","Café Special Green Sweet (250 gm)","7","1","120","0","0","Caf? Special Green Sweet (250 gm)","2019-01-23 12:00:23","1","2019-01-23 12:00:23","1");





CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `tmp_type` int(11) DEFAULT NULL,
  `default_temp` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO templates VALUES("1","jet_tmp","1","0","1");
INSERT INTO templates VALUES("2","api","0","0","1");
INSERT INTO templates VALUES("7","kartiano_web","1","1","1");
INSERT INTO templates VALUES("8","restaurant","0","1","1");





CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `state_id` varchar(255) DEFAULT NULL,
  `city_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(255) DEFAULT NULL,
  `post` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `reporting` varchar(255) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `utype` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO users VALUES("1","admin","admin","Admin","9411950511","admin@gmail.com","","1","1","1","","1/1imagePenguins.jpg","01/02/2017","Administrator","1");
INSERT INTO users VALUES("2","EMP001","admin","Nagesh rai","8382936646","M.D","4","2","1","2","1","","","Employee","1");
INSERT INTO users VALUES("3","EMP004","123456","Gaurav Kumar Singh","73763860056","gaurav.s@softgentechnologies.com","","","","3","","","2018-12-17","Employee","1");



