<div class="panel panel-default">
   <div class="col-md-6" style="padding:0px;">
   </div>
   <?php if($_SESSION['utype']=='Administrator'){ ?>
   <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- <center><h2>Test</h2></center> -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-green">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT `id` FROM `users` WHERE `utype`='Employee' AND `status`=1;")); ?></h3>
                  <p>Active Staffs</p>
               </div>
               <div class="icon">
                  <i class="fa fa-users"></i>
               </div>
               <a href="index.php?control=staff&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-red">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `product_category` WHERE 1;")); ?></h3>
                  <p>Product Category</p>
               </div>
               <div class="icon">
                  <i class="fa fa-hashtag"></i>
               </div>
               <a href="index.php?control=product&task=product_category" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-blue">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `product_list` WHERE 1;")); ?></h3>
                  <p>Products </p>
               </div>
               <div class="icon">
                  <i class="fa fa-cutlery"></i>
               </div>
               <a href="index.php?control=product&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-aqua">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `bill_fare` WHERE `date_created` LIKE '".date('Y-m-d')."%';")); ?></h3>
                  <p>Today's Bills</p>
               </div>
               <div class="icon">
                  <i class="fa fa-list-alt"></i>
               </div>
               <a href="index.php?control=billing&task=show&day=today" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-purple">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `bill_fare` WHERE 1")); ?></h3>
                  <p>All Bills</p>
               </div>
               <div class="icon">
                  <i class="fa fa-file-text"></i>
               </div>
               <a href="index.php?control=billing&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-yellow">
               <div class="inner">
                  <h3><i class="fa fa-inr"></i> <?php  $total =  mysql_fetch_array(mysql_query("SELECT sum(`grand_total`) as `total_sales` FROM `bill_fare` WHERE `status`=1 AND `date_created` LIKE '".date('Y-m-d')."%'")); 
                  echo $this->moneyFormatIndia(round($total['total_sales']?$total['total_sales']:0)); ?>/- </h3>
                  <p>Today's Sales</p>
               </div>
               <div class="icon">
                  <i class="fa fa-inr"></i>
               </div>
               <a href="index.php?control=billing&task=show&day=today" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-green">
               <div class="inner">
                 <h3><i class="fa fa-inr"></i> <?php  $total =  mysql_fetch_array(mysql_query("SELECT sum(`grand_total`) as `total_sales` FROM `bill_fare` WHERE `status`=1 ")); echo $this->moneyFormatIndia(round($total['total_sales']?$total['total_sales']:0)); ?>/- </h3>
                  <p>Total Sales</p>
               </div>
               <div class="icon">
                  <i class="fa fa-money"></i>
               </div>
               <a href="index.php?control=billing&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-red">
               <div class="inner">
                  <h3><i class="fa fa-info-circle"></i></h3>
                  <p>DayBook</p>
               </div>
               <div class="icon">
                  <i class="fa fa-book"></i>
               </div>
               <a href="index.php?control=report&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>

      </div>
      <div class="row">
          <hr>
                  <center><h4><strong>Day book Sales By Payment Mode: </strong></h4></center><hr>
               <!-- ================================================================ -->
               <!-- ================================================================ -->
               <!-- ================================================================ -->
               <!-- ./col -->
               <?php 
               $query = mysql_query("SELECT * FROM `payment_mode` WHERE 1 ORDER BY `mode` ASC"); 
                     $i==0;
                  while($result = mysql_fetch_array($query)){
                     $i++;
               ?>
               <div class="col-lg-3 col-xs-6 payment_mode"> 
                  <div class="small-box num<?php echo $i; ?>">
                     <div class="inner">
                        <h3 style="font-size: 30px;"><i class="fa fa-inr"></i> <?php echo $this->moneyFormatIndia($this->daybook_totalSell($result['id'])); ?>/-</h3>
                        <p><?php echo $result['mode']; ?></p>
                     </div>
                     <div class="icon">
                        <i class="fa fa-inr"></i>
                        <!-- <i class="fa fa-money"></i> -->
                     </div>
                     <a target="_blank" href="index.php?control=report&task=show&search=1&select_paymode=<?php echo $result['id']; ?>&from_date=<?php echo date('Y-m-d');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
               </div>
               <?php } ?>
               <!-- ./col -->
               <!-- ================================================================ -->
               <!-- ================================================================ -->
               <!-- ================================================================ -->         
      </div>
      <!-- /.row -->
   </section>
   <?php }else if($_SESSION['post']!='3'){ ?>
   <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- <center><h2>Test</h2></center> -->
       <!--   <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-green">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT `id` FROM `users` WHERE `utype`='Employee' AND `status`=1;")); ?></h3>
                  <p>Active Staffs</p>
               </div>
               <div class="icon">
                  <i class="fa fa-users"></i>
               </div>
               <a href="index.php?control=staff&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div> -->
         <!-- ./col -->

         <!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-blue">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `product_list` WHERE 1;")); ?></h3>
                  <p>Products </p>
               </div>
               <div class="icon">
                  <i class="fa fa-cutlery"></i>
               </div>
               <a href="index.php?control=product&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-aqua">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `bill_fare` WHERE `date_created` LIKE '".date('Y-m-d')."%';")); ?></h3>
                  <p>Today's Bills</p>
               </div>
               <div class="icon">
                  <i class="fa fa-list-alt"></i>
               </div>
               <a href="index.php?control=billing&task=show&day=today" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-purple">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `bill_fare` WHERE 1")); ?></h3>
                  <p>All Bills</p>
               </div>
               <div class="icon">
                  <i class="fa fa-file-text"></i>
               </div>
               <a href="index.php?control=billing&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-yellow">
               <div class="inner">
                  <h3><i class="fa fa-inr"></i> <?php  $total =  mysql_fetch_array(mysql_query("SELECT sum(`grand_total`) as `total_sales` FROM `bill_fare` WHERE `status`=1 AND `date_created` LIKE '".date('Y-m-d')."%'")); echo number_format(round($total['total_sales']?$total['total_sales']:0)); ?> </h3>
                  <p>Today's Sales</p>
               </div>
               <div class="icon">
                  <i class="fa fa-inr"></i>
               </div>
               <a href="index.php?control=billing&task=show&day=today" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <!-- ./col -->
         <!-- <div class="col-lg-3 col-xs-6">
           
            <div class="small-box bg-green">
               <div class="inner">
                 <h3><i class="fa fa-inr"></i> <?php  $total =  mysql_fetch_array(mysql_query("SELECT sum(`grand_total`) as `total_sales` FROM `bill_fare` WHERE `status`=1 ")); echo number_format(round($total['total_sales']?$total['total_sales']:0)); ?> </h3>
                  <p>Total Sales</p>
               </div>
               <div class="icon">
                  <i class="fa fa-money"></i>
               </div>
               <a href="index.php?control=billing&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div> -->
         <!-- ./col -->
        <!--  <div class="col-lg-3 col-xs-6">
         
            <div class="small-box bg-red">
               <div class="inner">
                  <h3><i class="fa fa-info-circle"></i></h3>
                  <p>DayBook</p>
               </div>
               <div class="icon">
                  <i class="fa fa-book"></i>
               </div>
               <a href="javascript:;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div> -->
         <!-- ./col -->
      </div>
      <!-- /.row -->
      
         
        <div class="row">
          <hr>
                  <center><h4><strong>Day book Sales By Payment Mode: </strong></h4></center><hr>
               <!-- ================================================================ -->
               <!-- ================================================================ -->
               <!-- ================================================================ -->
               <!-- ./col -->
               <?php 
               $query = mysql_query("SELECT * FROM `payment_mode` WHERE 1 ORDER BY `mode` ASC"); 
                     $i==0;
                  while($result = mysql_fetch_array($query)){
                     $i++;
               ?>
               <div class="col-lg-3 col-xs-6 payment_mode"> 
                  <div class="small-box num<?php echo $i; ?>">
                     <div class="inner">
                        <h3 style="font-size: 30px;"><i class="fa fa-inr"></i> <?php echo $this->moneyFormatIndia($this->daybook_totalSell($result['id'])); ?>/-</h3>
                        <p><?php echo $result['mode']; ?></p>
                     </div>
                     <div class="icon">
                        <i class="fa fa-inr"></i>
                        <!-- <i class="fa fa-money"></i> -->
                     </div>
                     <a target="_blank" href="index.php?control=report&task=show&search=1&select_paymode=<?php echo $result['id']; ?>&from_date=<?php echo date('Y-m-d');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
               </div>
               <?php } ?>
               <!-- ./col -->
               <!-- ================================================================ -->
               <!-- ================================================================ -->
               <!-- ================================================================ -->         
      </div>
      <!-- /.row -->
      
      
   </section>   
   <?php }else{ ?>
   <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-blue">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `product_list` WHERE `status`=1")); ?></h3>
                  <p>Products </p>
               </div>
               <div class="icon">
                  <i class="fa fa-cutlery"></i>
               </div>
               <a href="index.php?control=product&task=show" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-3 col-xs-6">
            
            <div class="small-box bg-aqua">
               <div class="inner">
                  <h3><?php  echo mysql_num_rows(mysql_query("SELECT * FROM `bill_fare` WHERE `date_created` LIKE '".date('Y-m-d')."%';")); ?></h3>
                  <p>Today's Bills</p>
               </div>
               <div class="icon">
                  <i class="fa fa-list-alt"></i>
               </div>
               <a href="index.php?control=billing&task=show&day=today" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
         </div>
      </div>
      <!-- /.row -->
   
      
   </section>  
   <?php } ?> 
   
</div>

<script type="text/javascript">
   $(document).ready(function(){
    //  $('.payment_mode:even div.small-box').addClass('bg-red');
     // $('.payment_mode:odd div.small-box').addClass('bg-green');

      n = $('.payment_mode').length;
      for(var i=0; i<=n; i++){
      if(i%3==0){
         $('.num'+i).addClass('bg-green');
         $('.num'+(i+1)).addClass('bg-aqua');
         $('.num'+(i+2)).addClass('bg-orange');
         $('.num'+(i+3)).addClass('bg-blue');
      }
   }
      /*else
      if(i%2==0){
        // $('.num'+i).removeClass('bg-red').removeClass('bg-green').addClass('bg-aqua');
         $('.num'+(i)).removeClass('bg-red').removeClass('bg-green').addClass('bg-aqua');
      }
      }*/
   });
</script>