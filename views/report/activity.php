<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style type="text/css">
   #data_table_wrapper .row .col-sm-6{
      float: right !important;
   }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Activity Log</h3>
            <?php foreach($results as $result) { }  ?>
           
            <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total : <?php echo $no_of_row; ?>
            </p>
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Activity Log</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <div class="col-md-5 col-sm-5">
                     <form action="" method="post" name="filterForm" id="filterForm" style="width:77px;">
                        <select name="filter" style="top: -5px;z-index: 999;" class="form-control"  id="filter" onchange="paging1(this.value)">
                           <option value="10000000"<?php if($_REQUEST['tpages']=="10000000") {?> selected="selected"<?php }?>>All</option>
                           <option value="10"<?php if($_REQUEST['tpages']=="10") {?> selected="selected"<?php }?>>10</option>
                           <option value="20"<?php if($_REQUEST['tpages']=="20") {?> selected="selected"<?php }?>>20</option>
                           <option value="50"<?php if($_REQUEST['tpages']=="50") {?> selected="selected"<?php }?>>50</option>
                           <option value="100"<?php if($_REQUEST['tpages']=="100") {?> selected="selected"<?php }?>>100</option>
                           <option value="500"<?php if($_REQUEST['tpages']=="500") {?> selected="selected"<?php }?>>500</option>
                         <!--   <option value="1000"<?php if($_REQUEST['tpages']=="1000") {?> selected="selected"<?php }?>>1000</option> -->
                        </select>
                        <input type="hidden" name="control" value="<?php echo $_REQUEST['control'];?>"  />
                        <input type="hidden" name="view" value="<?php echo $_REQUEST['view'];?>"  />
                        <input type="hidden" name="task" id="task" value="<?php echo $_REQUEST['task'];?>"  />
                        <input type="hidden" name="tpages" id="tpages" value=""  />
                        <input type="hidden" name="adjacents" value="<?php echo $_REQUEST['adjacents'];?>"  />
                        <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                        <input type="hidden" name="tmpid" value="<?php echo $_REQUEST['tmpid'];?>"  />
                        <input type="hidden" name="id" id="id"  />
                        <input type="hidden" name="status" id="status"  />
                        <input type="hidden" name="defftask" id="defftask" value="activity" />
                        <input type="hidden" name="del" id="del" />
                        <input type="hidden" name="edit" id="edit" />
                        <input type="hidden" name="view" id="view" />
                        <input type="hidden" name="from_date" value="<?php echo $_REQUEST['from_date']?$_REQUEST['from_date']:'';?>" />     
                        <input type="hidden" name="to_date" value="<?php echo $_REQUEST['to_date']?$_REQUEST['to_date']:'';?>" />
                        <input type="hidden" name="search" value="<?php echo $_REQUEST['search']?$_REQUEST['search']:'';?>" />       
                     </form>
                  </div>
                  <table id="data_table" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div></th>
                           <th><div align="center">System IP</div></th>
                           <th><div align="center">Activity</div></th>
                           <th><div align="center">Date Time</div></th>
                           <th><div align="center">Action Performed By</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           $user = mysql_fetch_array(mysql_query("SELECT * FROM `users` WHERE `id`='".$result['user_id']."'"));
                           $activity_date = date_create($result['date_created']);
                           
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>
                           <td align="center"><?php echo $result['system_ip'];?></td>
                           <td align="left"><?php echo $result['activity'];?></td>
                           <td align="center"><?php echo date_format($activity_date,'jS M y (g:i A)');?></td>
                           <td align="center"><?php echo $user['name'];?></td>
                        </tr>

                        <?php }  }else{?>

                        <?php } ?>       
                     </tbody>
                
              
                  </table>
                  <div class="clearfix"></div>
                  <?php if(count($uresults)>0){ 
                include("pagination.php");
                echo paginate_three($reload, $page, $tpages, $adjacents,$tdata); ?>
                <?php }?>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

