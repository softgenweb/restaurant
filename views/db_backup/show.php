<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Data</h3>
            <?php foreach($results as $result) { }  ?>
            <a href="index.php?control=db_backup&task=dbdata_access" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Data</a>
           
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Data List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15">
                              <div align="center">S.No</div>
                           </th>
                          <!-- <th>
                              <div align="center">Name</div>
                           </th>-->
                           <th>
                              <div align="center">Open File</div>
                           </th>
                         
                        </tr>
                     </thead>
                     <tbody>                     
                          
                            <?php  $directory = "db_backup/"; 
							$dir = "db_backup/";
                            //get all text files with a .txt extension.
                            $texts = glob($directory . "*.sql");
                            //print each file name
                            $i=1;
                            foreach($texts as $text){ ?>
                            <tr><td  align="center"><?php echo $i;?></td>
                           <!-- <td  align="center">
                            <?php   echo $text;?></td>-->
                        
                             <td  align="center"><a href="<?php  echo $text;?>" target='_blank'><?php  $data =  str_replace('db_backup/','',$text);
							 echo "File - ". $i .'# '. str_replace('.sql','',$data);
							 ?></a>
                          <!-- <input type="file" value="<?php  echo $directory.$text;?>">-->
                          </td>
                            </tr>
                            <?php $i++;						
							}                            
                            ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(5000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

