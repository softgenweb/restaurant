<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Product</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=product&task=show"><i class="fa fa-list" aria-hidden="true"></i> Product List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Product</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Product</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php   	
   	unset($_SESSION['alertmessage']);
    unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Item Name</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['product_name']; ?>" id="product_name" name="product_name" class="form-control"  required="">                     
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Category</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="category_id" id="category_id" class="form-control">
                        <option value="">Select</option>
                        <?php $query = mysql_query("SELECT * FROM `product_category` WHERE `status`=1"); 
                        while($category = mysql_fetch_array($query)){?>
                        <option value="<?php echo $category['id'] ?>" <?php echo $category['id']==$result['category_id']?'selected':''; ?>><?php echo $category['name'] ?></option>
                        <?php } ?>
                     </select>                    
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Quantity</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['qty']?$result['qty']:'1'; ?>" id="qty" name="qty" class="form-control"  required="" pattern="[0-9]+"
                        >                     
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Rate</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['price']; ?>" id="price" name="price" class="form-control"  required="" pattern="[0-9]+" onkeyup="discount_type()" onblur="discount_type()" onfocus="discount_type()">                     
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Discount Type</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select id="disc_type" name="disc_type" class="form-control"  required="" onchange="discount_type()">
                     	<option value="">Select</option>
                     	<?php $sql = mysql_query("SELECT * FROM `bill_discount` WHERE `status`=1");
                    while($disc = mysql_fetch_array($sql)){ ?>
                      <option value="<?php echo $disc['percent']; ?>" <?php if($result['discount_type']==$disc['percent']){ echo 'selected';}?>><?php echo $disc['percent']!='Other Amount'?$disc['percent'].'%':$disc['percent']; ?></option>
                    <?php } ?>
                     </select>                     
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Discount</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['discount'];?>" id="discount" name="discount" class="form-control" pattern="[0-9]+" readonly="readonly">                     
                  </div>
               </div>
               <div class="clearfix"></div>
                <div class="col-md-3">
                  <div class="form-group center_text">
                     <label>Remark</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <textarea id="remark" name="remark" class="form-control" ><?php echo $result['remark']; ?></textarea>                     
                  </div>
               </div>
               <div class="col-md-9 col-sm-8 col-xs-12">
               <div class="clearfix"></div>
                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="product"/>
               <!-- <input type="hidden" name="edit" value="1"/> -->
               <input type="hidden" name="task" value="save"/>
               <input type="hidden" name="view" value="show"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<script type="text/javascript">
      function discount_type() {   
	  var str = document.getElementById('disc_type').value;
	  
	     if(str){ 
				if(str=='Other Amount'){
				/*document.getElementById('discount').readOnly = false; 
				document.getElementById('discount').required  = 'required'; */
				
				 $('#discount').prop('readOnly', false).prop('required', true);
				document.getElementById('discount').value = '';			 
				}
				else{
					var price = $('#price').val();
					var discount_amount = ((price*str) / 100).toFixed(0);
				document.getElementById('discount').readOnly = true; 
				document.getElementById('discount').value = discount_amount;			 
				
				}
		 }else{
			document.getElementById('discount').value = ''; 
			document.getElementById('discount').readOnly = true; 
		 }
	  
	  
	  }
 
 
   function validation() {   
   	var chk=1;
   	
   		if(document.getElementById('country_name').value == '') { 
   			document.getElementById('msgcountry_name').innerHTML = "*Required field.";
   			chk=0;
   		}
   		else if(!isletter(document.getElementById('country_name').value)) { 
   			document.getElementById('msgcountry_name').innerHTML = "*Enter Valid Country Name.";
   			chk=0;
   		}
   		
   		else {
   			document.getElementById('msgcountry_name').innerHTML = "";
   		}
   
   			if(chk)
   			 {			
   				return true;		
   			}		
   			else 
   			{		
   				return false;		
   			}	
   		
   		
   		}
   
</script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
   	$(this).alert('close');
   });
   
     
   function goBack() {
      window.history.back();
   }
</script>

