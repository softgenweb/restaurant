<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Product</h3>
            <?php foreach($results as $result) { }  ?>
             <?php if($_SESSION['post']!='3'){ ?>
            <a href="index.php?control=product&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Product</a><?php } ?>
           <!--  <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total Country : <?php echo $no_of_row; ?>
            </p> -->
            <a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_final_report.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a>               
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Product List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div> </th>
                            <th><div align="center">Item</div></th>
                           <th> <div align="center">Category</div></th>
                           <th> <div align="center">Price</div></th>
                           <th> <div align="center">Discount %</div></th>
                           <th> <div align="center">Discount Amount</div></th>
                           <th><div align="center">Remark</div> </th>
                           <th><div align="center">Total Sales</div> </th>
                           <?php if($_SESSION['post']!='3'){ ?>
                           <th> <div align="center">Action</div></th>
                        <?php } ?>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                          
                           
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>
                           <!--  <td align="center"><?php echo $result['username'];?></td>   -->                   
                           <td align="left"><a href="index.php?control=product&task=product_sales&id=<?php echo $result['id']; ?>"><strong><?php echo $result['product_name'];?></strong></a></td>
                           <td align="center"><?php echo $this->productCategory($result['category_id']);?></td>
                           <td align="center"><?php echo '<i class="fa fa-inr"></i> '. $result['price'];?>/-</td>
                           <td align="center"><?php echo  $result['discount_type']!='Other Amount'?$result['discount_type'].'%':$result['discount_type'];?></td>                          
                           <td align="center"><?php echo '<i class="fa fa-inr"></i> '. $result['discount'];?>/-</td>
                           <td align="left"><?php echo $result['remark']?$result['remark']:'N/A';?></td>
                           <td align="center"><?php echo $this->moneyFormatIndia($this->ItemSale($result['id'])); ?>/-</td>
                            <?php if($_SESSION['post']!='3'){ ?>
                           <td align="center">
                              <a href="index.php?control=product&task=addnew&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp;
                              <?php
                                 if($result['status']==1){  ?>
                              <a href="index.php?control=product&task=status&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
                              <?php } else { ?>
                              <a href="index.php?control=product&task=status&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:Confirm;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
                              <?php } ?>

                           </td><?php } ?>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

