<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style type="text/css">
  table tfoot tr th{
    text-align: center !important;
  }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Product Sales</h3>
            <?php foreach($results as $result) { }  ?>
            
           <!--  <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total Country : <?php echo $no_of_row; ?>
            </p> -->
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li ><a href="index.php?control=product&task=show"><i class="fa fa-list" aria-hidden="true"></i> Product List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Product Sales</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                 <center><div class="col-md-12 ">
                    <h3  style="margin-top: 0px;"><strong><?php echo $this->ItemName($_REQUEST['id']); ?></strong></h3>
                <form method="POST" action="" name="search_form" id="search_form" autocomplete="off">
                     <div class="form-group col-md-2"></div>
                     <div class="form-group col-md-2"></div>
                     <div class="form-group col-md-2">
                        <input type="text" value="<?php echo $_REQUEST['from_date']; ?>" placeholder="From Date " id="from_date" name="from_date" class="form-control select_date" readonly="">
                      </div>
                      <div class="form-group col-md-2">
                        <input type="text" value="<?php echo $_REQUEST['to_date']; ?>" placeholder="To Date" id="to_date" name="to_date" class="form-control select_date" readonly="">
                      </div>
                      <!-- <div class="form-group col-md-2">
                        <select class="form-control" name="select_paymode" id="select_paymode">
                           <option value="">Payment Mode</option>
                           <?php $sql = mysql_query("SELECT * FROM `payment_mode` WHERE `status`=1 ORDER BY `mode` ASC");
                              while($pay = mysql_fetch_array($sql)){ ?>
                           <option value="<?php echo $pay['id']; ?>" <?php echo $_REQUEST['select_paymode']==$pay['id']?'selected':''; ?>><?php echo $pay['mode']; ?></option>
                           <?php } ?>
                        </select>
                      </div> -->
                      <div class="form-group col-md-2">
                        <input type="submit" value="Search" placeholder="Name" id="search" name="search" class="btn btn-primary bulu">
                        <input type="hidden" name="control" value="product">
                        <input type="hidden" name="task" value="product_sales">
                        <input type="hidden" name="show" value="product_sales">
                        <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>">
                      </div>
                  </form> 
                
                <!--  <div class="col-md-4"><label>Product Name : </label><?php echo $result['product_name']; ?></div>
                  <div class="col-md-4"><label></label></div>
                  <div class="col-md-4"><label></label></div> -->
                </center>
                </div>
               <div class="clearfix"></div>
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div> </th>
                            <th><div align="center">Bill No</div></th>
                           <th> <div align="center">Customer</div></th>
                           <th> <div align="center">Rate</div></th>
                           <th> <div align="center">Qty</div></th>
                           <th><div align="center">Discount</div> </th>
                           <th><div align="center">Amount</div> </th>
                           <th><div align="center">Date</div> </th>
                           <th> <div align="center">View</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                          
                            $originalDate = $result['bill_date'];
                              $bill_date = date("d-M-Y", strtotime($originalDate));
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>                   
                           <td align="center"><?php echo $result['bill_no'];//$result['product_name'];?></td>
                           <td align="center"><?php echo $this->CustomerName($result['bill_no']);?></td>
                           <td align="center"><?php echo '<i class="fa fa-inr"></i> '. $result['rate'];?>/-</td>
                           <td align="center"><?php echo $result['qty'];?></td>
                           <td align="center"><?php echo  '<i class="fa fa-inr"></i> '.$result['discount'];?>/-</td>                          
                           <td align="center"><?php echo '<i class="fa fa-inr"></i> '. $result['amount'];?>/-</td>
                           <td align="center"><?php echo $bill_date; ?></td>
                           <td align="center"><a class="btn btn-primary bulu" href="index.php?control=billing&task=view_bill&bno=<?php echo $result['bill_no']; ?>">View</a></td>
                         
                        </tr>
                        <?php $total +=$result['amount'];  
                        $qty +=$result['qty'];  
                        $disc +=$result['discount']; }  } ?>
                     </tbody>
                     <tfoot>
                       <tr>
                         <th></th>
                         <th colspan="3">Total</th>
                         
                         <th><?php echo $qty; ?></th>
                         <th><i class="fa fa-inr"></i> <?php echo $disc; ?>/-</th>
                         <th><i class="fa fa-inr"></i> <?php echo $total; ?>/-</th>
                         <th></th>
                       </tr>
                     </tfoot>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script type="text/javascript">
   $('.select_date').datetimepicker({
     yearOffset:0,
     lang:'ch',
     timepicker:false,
     format:'Y-m-d',
     formatDate:'Y/m/d',
     // formatDate: new Date(),
     maxDate:'+1970/01/01',
   
   
   });
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

