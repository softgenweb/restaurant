<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Bill</h3>
   </div>
   <style type="text/css">
      table th, table td{
      text-align: center;
      }
      table tfoot tr th{
      vertical-align: middle !important;
      }
      .select2-container .select2-selection--single{
      height: 34px !important;
      }
   </style>
   <link rel="stylesheet" href="template/restaurant/plugins/daterangepicker/daterangepicker-bs3.css">
   <link rel="stylesheet" href="template/restaurant/plugins/select2/select2.min.css">
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=billing&task=show"><i class="fa fa-list" aria-hidden="true"></i> Bill List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Bill</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Bill</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }
      $product = mysql_query("SELECT * FROM `product_list` WHERE `status`=1");
      
      ?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="col-md-12">
            <div class="col-md-4"><label>Customer Name: </label>
               <input type="text" name="customer_name" class="form-control">
            </div>
            <div class="col-md-4"><label>Mobile: </label>
               <input type="text" name="mobile" maxlength="10" pattern="[6789][0-9]{9}" class="form-control chk_number" required="">
            </div>
            <div class="col-md-4"><label> Date of Birth/ Anniversary:  </label>
               <input type="text" name="dob" id="dob" class="form-control" readonly="">
            </div>
         </div>
         <div class="clearfix"></div>
         <hr>
         <div class="row col-md-12 table-responsive">
            <div id="error" style="display: none;">
               <div id="myModal" class="modal fade" role="dialog" style="display: block;">
                  <div class="modal-dialog">
                     <center>
                        <div class="alert alert-danger alert-dismissable" style="margin-top: 40%;">
                           <strong>You Can not add Itmes more than 50</strong>
                        </div>
                     </center>
                  </div>
               </div>
            </div>
            <table class="table table-bordered table-striped">
               <thead>
                  <tr>
                     <th width="50">Sl No</th>
                     <th width="350">Items</th>
                     <th>Qty</th>
                     <th>Rate (<i class="fa fa-inr"></i>)</th>
                     <th>Discount (<i class="fa fa-inr"></i>)</th>
                     <th>Amount (<i class="fa fa-inr"></i>)</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody id="add_items">
                  <tr>
                     <td><strong class="slno">1</strong></td>
                     <td>
                        <select name="items[]" onchange="change_item(this.value,1);" class="form-control items select2" style="width: 100%;" required="">
                           <option value="">Select Items</option>
                           <?php while($items = mysql_fetch_array($product)){ ?>
                           <option value="<?php echo $items['id']; ?>"><?php echo $items['product_name']; ?></option>
                           <?php } ?>
                        </select>
                        <input type="hidden" value="" name="item_name[]" id="item_name1" class="form-control item_name" readonly="">
                     </td>
                     <td><input type="text" value="0" onkeyup="change_qty(this.value,1);" maxlength="5" name="qty[]" id="qty1" class="form-control qty chk_number" required=""></td>
                     <td><input type="text" value="0" name="rate[]" id="rate1" class="form-control rate" readonly=""></td>
                     <td><input type="text" value="0" name="discount[]" id="discount1" class="form-control discount" readonly="">   </td>
                     <td><input type="text" value="0" name="amount[]" id="amount1" class="form-control amount t_amount" readonly=""></td>
                     <td><a href="javascript:;" class="remove" id="remove1" style="display: none;"><i class="fa fa-minus-circle fa-2x"></i></a></td>
                  </tr>
               </tbody>
               <tfoot>
                  <tr valign="middle" align="center" style="border-top:2px solid #bdbdbd; ">
                     <th colspan="3"></th>
                     <th style="text-align: right;">Total (<i class="fa fa-inr"></i>):</th>
                     <th></th>
                     <th><input type="text" value="0" name="total_amt" id="total_amt" class="form-control" readonly=""></th>
                     <th style="text-align: left;">/-</th>
                  </tr>
                  <tr valign="middle" align="center" >
                     <th colspan="2"></th>
                     <th><a href="javascript:;" class=""><i class="fa fa-plus-circle fa-2x add_more" style="cursor: pointer;"></i></a></th>
                     <th style="text-align: right;">Tax (%):</th>
                     <th>
                        <select class="form-control" name="select_tax" id="select_tax" required="">
                           <option value="">Select</option>
                           <?php $sql = mysql_query("SELECT * FROM `bill_tax` WHERE `status`=1");
                              while($tax = mysql_fetch_array($sql)){ ?>
                           <option value="<?php echo $tax['percent']; ?>"><?php echo $tax['percent']!='Other Amount'?$tax['percent'].'%':$tax['percent']; ?></option>
                           <?php } ?> 
                        </select>
                     </th>
                     <th><input type="text" value="0" onkeyup="change_tax();" placeholder="Enter Amount" name="total_tax" id="total_tax" class="form-control chk_number" readonly=""></th>
                     <th style="text-align: left;">/-</th>
                  </tr>
                  <tr valign="middle" align="center" >
                     <th colspan="3"></th>
                     <th style="text-align: right;">Discount (%):</th>
                     <th>
                        <select class="form-control" name="select_disc" id="select_disc" required="">
                           <option value="">Select</option>
                           <?php $sql = mysql_query("SELECT * FROM `bill_discount` WHERE `status`=1");
                              while($disc = mysql_fetch_array($sql)){ ?>
                           <option value="<?php echo $disc['percent']; ?>"><?php echo $disc['percent']!='Other Amount'?$disc['percent'].'%':$disc['percent']; ?></option>
                           <?php } ?>
                        </select>
                     <th><input type="text" value="0" onkeyup="change_disc();" placeholder="Enter Amount" name="total_disc" id="total_disc" class="form-control chk_number" readonly=""></th>
                     </th>
                     <th style="text-align: left;">/-</th>
                  </tr>
                  <tr valign="middle" align="center" >
                     <th ></th>
                     <th style="text-align: right;">Payment Mode</th>
                     <th >
                        <select class="form-control" name="select_paymode" id="select_paymode" required="">
                           <option value="">Select</option>
                           <?php $sql = mysql_query("SELECT * FROM `payment_mode` WHERE `status`=1 ORDER BY `mode` ASC");
                              while($pay = mysql_fetch_array($sql)){ ?>
                           <option value="<?php echo $pay['id']; ?>"><?php echo $pay['mode']; ?></option>
                           <?php } ?>
                        </select>
                     </th>
                     <th style="text-align: right;">Grand Total (<i class="fa fa-inr"></i>):</th>
                     <th></th>
                     <th><input type="text" name="grand_total" value="0" id="grand_total" class="form-control" readonly=""></th>
                     <th style="text-align: left;">/-</th>
                  </tr>
                  <!-- <tr>
                     <th colspan="2"></th>
                     <th><i class="fa fa-plus-circle fa-2x add_more" style="cursor: pointer;"></i> </th>
                     <th colspan="3"></th>
                     
                     </tr> -->
               </tfoot>
            </table>
            <div>
               <center>
                  <input type="submit" class="btn btn-primary bulu" name="submit" value="Save Bill">
                  <input type="hidden" name="control" value="billing">
                  <input type="hidden" name="task" value="save">
                  <input type="hidden" name="view" value="show">
                  <input type="hidden" name="total_item"  id="total_item" value="1">
                  <input type="hidden" name="total_qty"  id="total_qty" value="1">
                  <input type="hidden" name="bill_id" value="<?php echo $result['id']; ?>">
               </center>
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<script src="template/restaurant/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript">
   $(".select2").select2();
   
   $('form').submit(function(){
   $(this).children('input[type=submit]').prop('disabled', true);
   });
   /*============Auto hide alert box================*/
   /* $(".alert").delay(2000).slideUp(200, function() {
   $(this).alert('close');
   });*/
   
   
   function goBack() {
   window.history.back();
   }
   
   $('.chk_number').keypress(function(evt){
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : event.keyCode;
   if (charCode > 31
   // if (charCode != 46 && charCode > 31
   && (charCode < 48 || charCode > 57))
   return false;
   
   return true;
   });
   
   /*=================Add Boxes=================*/
   $('.add_more').click(function(){
   n = $('tbody#add_items tr').length +1;
   // alert(n);
   if(n>50){
   show_error();
   // alert("You Can not add Itmes more than 50");
   return 0;
   }else{
   var html_box = '<tr><td><strong class="slno">'+n+'</strong></td><td><select name="items[]" onchange="change_item(this.value,'+n+');" class="form-control items select2" id="items'+n+'" style="width: 100%;" required=""><option value="">Select Items</option><?php $product = mysql_query("SELECT * FROM `product_list` WHERE `status`=1"); while($items = mysql_fetch_array($product)){ ?><option value="<?php echo $items["id"]; ?>"><?php echo $items["product_name"]; ?></option><?php } ?></select><input type="hidden" value="" name="item_name[]" id="item_name'+n+'" class="form-control item_name" readonly=""></td><td><input type="text" value="0" onkeyup="change_qty(this.value,'+n+');" maxlength="5" name="qty[]" id="qty'+n+'" class="form-control qty chk_number" required=""></td><td><input type="text" value="0" name="rate[]" id="rate'+n+'" class="form-control rate" readonly=""></td><td><input type="text" value="0" name="discount[]" id="discount'+n+'" class="form-control discount" readonly=""></td><td><input type="text" value="0" name="amount[]" id="amount'+n+'" class="form-control amount t_amount" value="" readonly=""></td><td><a href="javascript:;" class="remove" id="remove'+n+'"><i class="fa fa-minus-circle fa-2x"></i></a></td></tr>';
   $('#add_items').append(html_box);
   $('#total_item').val(n);
   
   }
   $(".select2").select2();
   $('.remove').click(function(){
   $(this).closest('tr').remove();
   $('#total_item').val(($('tbody#add_items tr').length));
   $('td strong.slno').text(function (i) { return i + 1; });
   /*==========================================*/
   $('td select.items').each(function(a){
    $(this).attr('id','items'+parseInt(a+1));
    $(this).attr('onchange','change_item(this.value,'+parseInt(a+1)+')');
   });
   /*==========================================*/
   $('td input.qty').each(function(b){
    $(this).attr('id','qty'+parseInt(b+1));
    $(this).attr('onkeyup','change_qty(this.value,'+parseInt(b+1)+')');
   });
   /*==========================================*/
   $('td input.rate').each(function(c){
    $(this).attr('id','rate'+parseInt(c+1));
   });
   /*==========================================*/
   $('td input.discount').each(function(d){
    $(this).attr('id','discount'+parseInt(d+1));
   });
   /*==========================================*/
   $('td input.amount').each(function(e){
    $(this).attr('id','amount'+parseInt(e+1));
   });
   /*==========================================*/
   change_qty(n, n)
   });

   $('.chk_number').keypress(function(evt){
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : event.keyCode;
   if (charCode > 31
   // if (charCode != 46 && charCode > 31
   && (charCode < 48 || charCode > 57))
   return false;
   
   return true;
   });
   });
   
   function show_error(){
   $('#error').show();
   $('#myModal').addClass('in'); 
   $("#error").delay(2000).slideUp(200, function() {
   $(this).alert('close'); 
   });
   }
   /*===========================================*/
   
   function change_item(val, sl){
   $.get("script/billing_items.php?pid="+val, function(data){
    // alert(data);
    value = data.split("#");
    $('#qty'+sl).val(value[0]);
    $('#rate'+sl).val(parseFloat(value[1]).toFixed(2));
    // $('#disc_old'+sl).val(value[2]);
    $('#discount'+sl).val(parseFloat(value[2]).toFixed(2));   
    $('#item_name'+sl).val(value[3].trim()); 
    change_qty(val,sl);
       

   });

   }
   
   
   function change_qty(val, sl){
   var discount = parseFloat($("#discount"+sl).val());
   var rate = parseFloat($("#rate"+sl).val());
   var qty = $("#qty"+sl).val()?parseFloat($("#qty"+sl).val()):0;
   var amount = parseFloat($("#amount"+sl).val()).toFixed(2);
   var tax = parseFloat($("#total_tax").val()).toFixed(2);
   var total_disc = parseFloat($("#total_disc").val()).toFixed(2);
   
   /*==============================*/
   /*$("#select_tax").val("");
   $("#total_tax").val("0").prop('readonly', true).prop('required', false);
   $("#select_disc").val("");
   $("#total_disc").val("0").prop('readonly', true).prop('required', false);*/
   /*==============================*/
   $('#amount'+sl).val((qty*(rate-discount)).toFixed(2));
   
   /*====Total Amount=====*/
   var amt_total = 0;
   $('.t_amount').each(function () {
      amt_total += parseFloat(this.value) || 0;
   $('#total_amt').val(amt_total.toFixed(2));
   
   });
   /*====Total Quantity=====*/
   var qty_total = 0;
   $('.qty').each(function () {
      qty_total += parseInt(this.value) || 0;
   $('#total_qty').val(qty_total);
   
   });
   
   change_gtotal();
   
   }
   
   
   /*==========on Change tax===========*/
   $("#select_tax").change(function(){
   val = $(this).val() ? $(this).val() : 0;
   // if(val == 'other'){
   if(val.match(/Other/g)){
   $('#total_tax').prop('readonly', false).prop('required', true).val(0);
   change_gtotal();
   
   }else{
   $('#total_tax').prop('readonly', true).prop('required', false);
   total_amt = parseFloat($('#total_amt').val());
   tax_percent = parseFloat($('#select_tax').val());
   disc_percent = parseFloat($('#select_disc').val());
   total_tax = parseFloat($('#total_tax').val());
   
   tax_amt = parseFloat(((total_amt?total_amt:0)*(tax_percent?tax_percent:0))/100).toFixed(2);
   $('#total_tax').val(tax_amt); 
   change_gtotal();
   }
   
   });
   
   /*==========on Change discount===========*/
   $("#select_disc").change(function(){
   val = $(this).val() ? $(this).val() :0;
   if(val.match(/Other/g)){
   $('#total_disc').prop('readonly', false).prop('required', true).val(0);
   
   // change_gtotal();
   change_disc();
   
   }else{
   $('#total_disc').prop('readonly', true).prop('required', false);
   total_amt = parseFloat($('#total_amt').val());
   tax_percent = parseFloat($('#select_tax').val());
   disc_percent = parseFloat($('#select_disc').val());
   total_tax = parseFloat($('#total_tax').val());
   // change_gtotal();
   total_disc = (((total_amt?total_amt:0)+(total_tax?total_tax:0))*(disc_percent?disc_percent:0))/100;
   $('#total_disc').val(parseFloat(total_disc).toFixed(2));
   change_gtotal();
   }
   
   
   
   });
   
   
   
   
   
   function change_gtotal(){
   total_amt = parseFloat($('#total_amt').val());
   tax_percent = parseFloat($('#select_tax').val());
   disc_percent = parseFloat($('#select_disc').val());
   total_tax = parseFloat($('#total_tax').val());
   
   total_disc = (((total_amt?total_amt:0)+(total_tax?total_tax:0))*(disc_percent?disc_percent:0))/100;
   $('#total_disc').val(parseFloat(total_disc).toFixed(2));
   
   total_tax = parseFloat(((total_amt?total_amt:0)*(tax_percent?tax_percent:0))/100).toFixed(2);
   $('#total_tax').val(total_tax); 
   
   // total_tax = parseFloat($('#total_tax').val()?$('#total_tax').val():0);
   // total_disc = parseFloat($('#total_disc').val()?$('#total_disc').val():0);
   
   
   
   g_total = ((parseFloat(total_amt)+parseFloat(total_tax?total_tax:0))-parseFloat(total_disc?total_disc:0));
   $('#grand_total').val(parseFloat(g_total).toFixed(2));
   
   }
   
   
   function change_tax(){
   total_amt = parseFloat($('#total_amt').val());
   total_tax = parseFloat($('#total_tax').val());
   total_disc = parseFloat($('#total_disc').val());
   
   total_disc = (((total_amt?total_amt:0)+(total_tax?total_tax:0))*(disc_percent?disc_percent:0))/100;
   $('#total_disc').val(parseFloat(total_disc).toFixed(2));
   
   g_total = ((parseFloat(total_amt)+parseFloat(total_tax?total_tax:0))-parseFloat(total_disc?total_disc:0));
   $('#grand_total').val(parseFloat(g_total).toFixed(2));
   
   }
   function change_disc(){
   total_amt = parseFloat($('#total_amt').val());
   // tax_percent = parseFloat($('#select_tax').val());
   total_disc = parseFloat($('#total_disc').val());
   total_tax = parseFloat($('#total_tax').val());
   
   g_total = ((parseFloat(total_amt?total_amt:0)+parseFloat(total_tax?total_tax:0))-parseFloat(total_disc?total_disc:0));
   $('#grand_total').val(parseFloat(g_total).toFixed(2));
   
   }
   
   
   
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<!-- <script src="assets/date_picker/jquery.js"></script>  -->
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script type="text/javascript">
   $('#dob').datetimepicker({
     yearOffset:0,
     lang:'ch',
     timepicker:false,
     format:'d-m-Y',
     formatDate:'Y/m/d',
     // formatDate: new Date(),
     maxDate:'+1970/01/01',
   
   
   });
</script>

