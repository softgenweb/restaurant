<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Bill</h3> 
            <a target="_blank" href="index.php?control=billing&task=print_bill&bno=<?php echo $_REQUEST['bno']; ?>" class="btn btn-primary pull-right" ><i class="fa fa-print" aria-hidden="true"></i> Print</a>         
            <!-- <a href="script/print_bill.php?bill=<?php echo $_REQUEST['bno']; ?>" class="btn btn-primary pull-right"><i class="fa fa-print" aria-hidden="true"></i> Print</a>          -->
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="index.php?control=billing&task=show"><i class="fa fa-list" aria-hidden="true"></i> Bills List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> View Bill</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']); }
         $bill = mysql_fetch_array(mysql_query("SELECT * FROM `bill_fare` WHERE `bill_no`='".$_REQUEST['bno']."'"));
         $originalDate = $bill['date_created'];
         $bill_date = date("d-M-Y", strtotime($originalDate));

         $payment_mode = mysql_fetch_array(mysql_query("SELECT * FROM `payment_mode` WHERE `id`='".$bill['payment_mode']."'"));
         $company = mysql_fetch_array(mysql_query("SELECT * FROM `company` WHERE 1"));
            ?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              <!-- From -->
              <address style="padding-left: 15px;">
              <strong><?php echo ucwords($company['name']); ?></strong><br>
              <?php echo ucwords($company['address']); ?><br>
              GST: <?php echo $company['gst_no']; ?> <br>
              Phone: <?php echo $company['phone']; ?> <br>
              Email: <?php echo $company['email']; ?>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <!-- To -->
              <address  style="padding-left: 15px;">
                <strong>Customer</strong><br>
               <?php //echo $bill['customer_name']?$bill['customer_name']."<br> ".$bill['mobile']."<br> ".$bill['dob']:$bill['mobile'];?>
               <?php echo $bill['customer_name']?"<b>Name:</b> ".$bill['customer_name']."<br> <b>Mobile No: </b> ".$bill['mobile']."<br> <b>Date of Birth/Anniversary: </b> ".($bill['dob']?date("d-M-Y", strtotime($bill['dob'])):'N/A'):"<b>Mobile No:</b> ".$bill['mobile']; ?>
              </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col ">
               <address  style="padding-left: 15px;">
              <b>Token No. #<?php echo $bill['token_no']; ?></b><br>
              <br>
              <b>Bill No:</b> <?php echo $bill['bill_no']; ?><br>
              <b>Bill Date:</b>  <?php echo $bill_date; ?><br>
              <b>Payment Mode:</b> <?php echo $payment_mode['mode']; ?>
            </address>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped ">
                <thead>
                  <tr>
                    <th><div align="center"><strong>Sl</strong></div></th>
                    <th><div align="center">Items</div></th>
                    <th><div align="center">Quantity</div></th>
                    <th><div align="center">Rate (<i class="fa fa-inr"></i>)</div></th>
                    <th><div align="center">Discount (<i class="fa fa-inr"></i>)</div></th>
                    <th><div align="center">Amount (<i class="fa fa-inr"></i>)</div></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if($results) {
                      $countno = ($page-1)*$tpages;
                      $i=0;
                      foreach($results as $result){ 
                      $i++;
                      $countno++;
                     ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                     $originalDate = $result['date_created'];
                     $bill_date = date("d-m-Y", strtotime($originalDate));
                  ?>
                  <tr>
                     <td align="center"><?php echo $countno; ?></td> 
                     <td align="center"><?php echo $result['product_name'];?></td>
                     <td align="center"><?php echo $result['qty'];?></td>
                     <td align="center"><?php echo $result['rate'];?>/-</td>
                     <td align="center"><?php echo $result['discount'];?>/-</td>
                     <td align="center"><?php echo $result['amount'];?>/-</td>
                  </tr>
                   <?php }  } ?>
                </tbody>
              </table> <hr>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <!-- accepted payments column -->
             <div class="col-xs-8">
             <!-- <p class="lead">Payment Methods:</p>
              <img src="../../dist/img/credit/visa.png" alt="Visa">
              <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
              <img src="../../dist/img/credit/american-express.png" alt="American Express">
              <img src="../../dist/img/credit/paypal2.png" alt="Paypal">
              <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
              </p> -->
            </div><!-- /.col -->
            <div class="col-xs-4 ">
              <!-- <p class="lead">Amount Due 2/22/2014</p> -->
              <div class="table-responsive">
                <table class="table ">
                  <tr>
                    <th align="center" style="width:50%">Subtotal:</th>
                    <td align="right"><i class="fa fa-inr"></i></td>
                    <td align="left"><?php echo $bill['total_amount']; ?>/-</td>
                  </tr>

                   <tr>
                      <th align="left">CGST (<?php echo $bill['tax_percent']!='Other Amount'?($bill['tax_percent']/2).'%':($bill['tax_percent']); ?>)</th>
                      <td align="right"><i class="fa fa-inr"></i></td>
                      <td align="left"><?php echo ($bill['total_tax']/2); ?>/-</td>
                   </tr>
                   <tr>
                      <th align="left">SGST (<?php echo $bill['tax_percent']!='Other Amount'?($bill['tax_percent']/2).'%':($bill['tax_percent']); ?>)</th>
                      <td align="right"><i class="fa fa-inr"></i></td>
                      <td align="left"><?php echo ($bill['total_tax']/2); ?>/-</td>
                   </tr>
                  <!-- <tr>
                    <th align="center">Tax (<?php echo $bill['tax_percent']!='Other Amount'?$bill['tax_percent'].'%':$bill['tax_percent']; ?>):</th>
                    <td align="right"><i class="fa fa-inr"></i>
                    <td align="left"><?php echo $bill['total_tax']; ?>/-</td>
                  </tr> -->
                  <tr>
                    <th align="center">Discount (<?php echo $bill['disc_percent']!='Other Amount'?$bill['disc_percent'].'%':$bill['disc_percent']; ?>):</th>
                    <td align="right"><i class="fa fa-inr"></i></td>
                    <td align="left"><?php echo $bill['total_discount']; ?>/-</td>
                  </tr>
                  <tr style="background-color: #d8d8d8b3;">
                    <th align="center">Total:</th>
                    <td align="right"><i class="fa fa-inr"></i></td>
                    <td align="left"><strong><?php echo $this->moneyFormatIndia(round($bill['grand_total'])); ?>/-</strong></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

