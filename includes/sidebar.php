<?php session_start();?>
<aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
   <div class="background_layer"></div>
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <div class="user-panel" style="padding-bottom: 48px;">
         <div class="pull-left image">
            <img src="images/user2-160x160.jpg" class="img-circle" alt="User Image"></a>
         </div>
         <div class="pull-left info" >
            <h3 style="margin:unset;"><?php echo $_SESSION['admin_name']; ?></h3>
            <a href="index.php?control=user&task=updateprofile&aid=<?php echo $_SESSION['adminid'];?>" title="Update Profile"><i class="fa fa-circle text-success"></i><b>Update Profile</b></a>&nbsp;</br>
            <a href="index.php?control=user&task=changepassword" title="Change Password"><i class="fa fa-circle text-success"></i><b>Change Password</b></a><br />
         </div>
      </div>   
   <?php if($_SESSION['utype']=='Administrator'){ ?>

      <ul class="sidebar-menu">
         <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
            <a href="#">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='company'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=company&task=show">
            <i class="fa fa-building" aria-hidden="true"></i><span>Company Info</span>              
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='payment'){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-money" aria-hidden="true"></i><span>Payment Master</span>
            </a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='pay_mode'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=pay_mode">Payment Mode</a></li>
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='show_tax'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=show_tax">TAX Master</a></li>
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='show_discount'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=show_discount">Discount Master</a></li>
              
            </ul>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='staff'){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-users" aria-hidden="true"></i><span>Staff Master</span>
            </a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='emp_post'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=emp_post">Staff Post</a></li>
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=addnew">Add Staff</a></li>
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=show">View Staff</a></li>
            </ul>
         </li>

         <li class="treeview <?php if($_REQUEST['control']=='product' ){ ?>active<?php } ?>">
         <!-- <li class="treeview <?php if($_REQUEST['control']=='product' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>"> -->
            <a href="javascript:;">
            <i class="fa fa-cutlery" aria-hidden="true"></i><span>Product Master</span>
            </a>          
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='product' && $_REQUEST['task']=='product_category'){ ?>class="active"<?php } ?>><a href="index.php?control=product&task=product_category">Product Category</a></li>
               <li <?php if($_REQUEST['control']=='product' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=product&task=addnew">Add Product</a></li>
               <li <?php if($_REQUEST['control']=='product' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=product&task=show">View Product</a></li>
            </ul>
         </li>

         <li class="treeview <?php if($_REQUEST['control']=='billing' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-file-text" aria-hidden="true"></i><span>Billing</span>
            </a>          
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='billing' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=billing&task=addnew">Add Bill</a></li>
               <li <?php if($_REQUEST['control']=='billing' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=billing&task=show">View Bills</a></li>
            </ul>
         </li>

         <li class="treeview <?php if($_REQUEST['control']=='report' ){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-info-circle" aria-hidden="true"></i><span>Report</span>
            </a>          
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show"> Billing Wise</a></li>
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='items_wise'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=items_wise">Item Wise</a></li>
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='profit_report'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=profit_report"> Final Report</a></li>
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='activity'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=activity"> Activity Log</a></li>
            </ul>
            
            
            <!--        
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show">Daybook</a></li>
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='activity'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=activity"> Activity Log</a></li>
            </ul> -->
         </li>
         
         
         <li class="treeview <?php if($_REQUEST['control']=='db_backup' && ($_REQUEST['task']=='dbdata_access' || $_REQUEST['task']=='show' || $_REQUEST['task']=='import_db')){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-file-text" aria-hidden="true"></i><span>DB Backup</span>
            </a>          
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='db_backup' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=db_backup">View Data File</a></li>
             <!--  <li <?php if($_REQUEST['control']=='db_backup' && $_REQUEST['task']=='dbdata_access'){ ?>class="active"<?php } ?>><a href="index.php?control=db_backup&task=dbdata_access">Export Data</a></li> -->   
               <li <?php if($_REQUEST['control']=='db_backup' && $_REQUEST['task']=='import_db'){ ?>class="active"<?php } ?>><a href="index.php?control=db_backup&task=import_db">Import Data</a></li>
            </ul>
         </li>

      </ul>
   
   <?php }else if($_SESSION['post']!='3'){ ?> 

      <ul class="sidebar-menu">
         <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
            <a href="#">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span>
            </a>
         </li>
         <!-- <li class="treeview <?php if($_REQUEST['control']=='company'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=company&task=show">
            <i class="fa fa-building" aria-hidden="true"></i><span>Company Info</span>              
            </a>
         </li> -->
         <li class="treeview <?php if($_REQUEST['control']=='payment'){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-money" aria-hidden="true"></i><span>Payment Master</span>
            </a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='pay_mode'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=pay_mode">Payment Mode</a></li>
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='show_tax'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=show_tax">TAX Master</a></li>
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='show_discount'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=show_discount">Discount Master</a></li>
              
            </ul>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='staff'){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-users" aria-hidden="true"></i><span>Staff Master</span>
            </a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='emp_post'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=emp_post">Staff Post</a></li>
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=addnew">Add Staff</a></li>
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=show">View Staff</a></li>
            </ul>
         </li>

         <li class="treeview <?php if($_REQUEST['control']=='product' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-cutlery" aria-hidden="true"></i><span>Product Master</span>
            </a>          
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='product' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=product&task=addnew">Add Product</a></li>
               <li <?php if($_REQUEST['control']=='product' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=product&task=show">View Product</a></li>
            </ul>
         </li>

         <li class="treeview <?php if($_REQUEST['control']=='billing' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-file-text" aria-hidden="true"></i><span>Billing</span>
            </a>          
            <ul class="treeview-menu">
               <!-- <li <?php if($_REQUEST['control']=='billing' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=billing&task=addnew">Add Bill</a></li> -->
               <li <?php if($_REQUEST['control']=='billing' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=billing&task=show">View Bills</a></li>
            </ul>
         </li>

         <!-- <li class="treeview <?php if($_REQUEST['control']=='report' ){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-info-circle" aria-hidden="true"></i><span>Report</span>
            </a>          
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show">Daybook</a></li>
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='activity'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=activity"> Activity Log</a></li>
            </ul>
         </li> -->

      </ul>
       
   <?php }else{ ?>

      <ul class="sidebar-menu">
         <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
            <a href="#">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span>
            </a>
         </li>
<!--          <li class="treeview <?php if($_REQUEST['control']=='company'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=company&task=show">
            <i class="fa fa-building" aria-hidden="true"></i><span>Company Info</span>              
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='payment'){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-money" aria-hidden="true"></i><span>Payment Master</span>
            </a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='pay_mode'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=pay_mode">Payment Mode</a></li>
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='show_tax'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=show_tax">TAX Master</a></li>
               <li <?php if($_REQUEST['control']=='payment' && $_REQUEST['task']=='show_discount'){ ?>class="active"<?php } ?>><a href="index.php?control=payment&task=show_discount">Discount Master</a></li>
              
            </ul>
         </li> -->
         <!-- <li class="treeview <?php if($_REQUEST['control']=='staff'){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-users" aria-hidden="true"></i><span>Staff Master</span>
            </a>
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='emp_post'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=emp_post">Staff Post</a></li>
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=addnew">Add Staff</a></li>
               <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=show">View Staff</a></li>
            </ul>
         </li> -->

         <li class="treeview <?php if($_REQUEST['control']=='product' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-cutlery" aria-hidden="true"></i><span>Products</span>
            </a>          
            <ul class="treeview-menu">
               <!-- <li <?php if($_REQUEST['control']=='product' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=product&task=addnew">Add Product</a></li> -->
               <li <?php if($_REQUEST['control']=='product' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=product&task=show">View Product</a></li>
            </ul>
         </li>

         <li class="treeview <?php if($_REQUEST['control']=='billing' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-file-text" aria-hidden="true"></i><span>Billing</span>
            </a>          
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='billing' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=billing&task=addnew">Add Bill</a></li>
               <li <?php if($_REQUEST['control']=='billing' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=billing&task=show">View Bills</a></li>
            </ul>
         </li>

         <!-- <li class="treeview <?php if($_REQUEST['control']=='report' ){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-info-circle" aria-hidden="true"></i><span>Report</span>
            </a>          
            <ul class="treeview-menu">
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show">Daybook</a></li>
               <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='activity'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=activity"> Activity Log</a></li>
            </ul>
         </li> -->

      </ul>
   
   <?php } ?> 
   <!-- /.sidebar -->
</section>
</aside>

