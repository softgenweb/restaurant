<?php session_start();?>


  <aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
  <div class="background_layer"></div>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    <div class="user-panel" style="padding-bottom: 48px;">
            <div class="pull-left image">
            <img src="images/user2-160x160.jpg" class="img-circle" alt="User Image"></a>
            </div>
            <div class="pull-left info" >
              <h3 style="margin:unset;"><?php echo $_SESSION['admin_name']; ?></h3>
             <a href="index.php?control=user&task=updateprofile&aid=<?php echo $_SESSION['adminid'];?>" title="Update Profile"><i class="fa fa-circle text-success"></i><b>Update Profile</b></a>&nbsp;</br>
             <a href="index.php?control=user&task=changepassword" title="Change Password"><i class="fa fa-circle text-success"></i><b>Change Password</b></a><br />
             
            </div>
          </div>
    
    
      <ul class="sidebar-menu">
        
        <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
          <a href="#">
             <i class="fa fa-indent" aria-hidden="true"></i>
				<span>Dashboard</span>
          </a>
        </li>
        
        <li class="treeview <?php if($_REQUEST['control']=='staff' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Staff Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=staff&task=addnew">Add Staff</a></li>
            <li <?php if($_REQUEST['control']=='staff' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=staff">View Staff</a></li>
            
          </ul>
        </li>
        
        <li class="treeview <?php if($_REQUEST['control']=='customer' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-users" aria-hidden="true"></i><span>Customer Master</span>
          </a>
          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='customer' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=customer&task=addnew">Add Customer</a></li>
            <li <?php if($_REQUEST['control']=='customer' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=customer&task=show">View Customer</a></li>
            
          </ul>
        </li>
        
        <li class="treeview <?php if($_REQUEST['control']=='time_slot' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-building" aria-hidden="true"></i><span>Time Slot Master</span>
          </a>          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='time_slot' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=time_slot&task=addnew">Add Time Slot</a></li>
            <li <?php if($_REQUEST['control']=='time_slot' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=time_slot&task=show">View Time Slot</a></li>          </ul>
        </li>
        
        <li class="treeview <?php if($_REQUEST['control']=='package' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-building" aria-hidden="true"></i><span>Package Master</span>
          </a>          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='package' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=package&task=addnew">Add Package</a></li>
            <li <?php if($_REQUEST['control']=='package' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=package&task=show">View Package</a></li>          </ul>
        </li>
        
        
      <!--  
         <li class="treeview <?php if($_REQUEST['control']=='assign_package' && $_REQUEST['task']=='show'){ ?>active active-menu<?php } ?>">
          <a href="index.php?control=assign_package&task=show">
             <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Assign Package</span>              
          </a>
        </li>-->
        
         <li class="treeview <?php if($_REQUEST['control']=='appointment' && $_REQUEST['task']=='show'){ ?>active active-menu<?php } ?>">
          <a href="index.php?control=appointment&task=show">
             <i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Appointment</span>              
          </a>
        </li>
        
        <li class="treeview <?php if($_REQUEST['control']=='assign_job' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-building" aria-hidden="true"></i><span>Assign Job to Staff</span>
          </a>          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='assign_job' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=assign_job&task=addnew">Add Assign Job to Staff</a></li>
            <li <?php if($_REQUEST['control']=='assign_job' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=assign_job&task=show">View Assign Job to Staff</a></li>         
             </ul>
        </li>
        
        <li class="treeview <?php if($_REQUEST['control']=='report' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
          <a href="javascript:;">
            <i class="fa fa-building" aria-hidden="true"></i><span>Report</span>
          </a>          
          <ul class="treeview-menu">
            <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=addnew">Staff Report</a></li>
            <li <?php if($_REQUEST['control']=='report' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=report&task=show">Customer  Report</a></li>         
             </ul>
        </li>
        
        
<!--  <li class="treeview <?php if($_REQUEST['control']=='regional_hierarchy' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show')){ ?>active<?php } ?>">
<a href="javascript:;">
<i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Country Master</span>
</a>

<ul class="treeview-menu">
<li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=addnew">Add Country</a></li>
<li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show">View Country</a></li>

</ul>
</li>

<li class="treeview <?php if($_REQUEST['control']=='regional_hierarchy' && ($_REQUEST['task']=='addnew_state' || $_REQUEST['task']=='show_state')){ ?>active<?php } ?>">
<a href="javascript:;">
<i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>State Master</span>
</a>

<ul class="treeview-menu">
<li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='addnew_state'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=addnew_state">Add State</a></li>
<li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_state'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_state">View State</a></li>

</ul>
</li>

<li class="treeview <?php if($_REQUEST['control']=='regional_hierarchy' && ($_REQUEST['task']=='addnew_city' || $_REQUEST['task']=='show_city')){ ?>active<?php } ?>">
<a href="javascript:;">
<i class="fa fa-building" aria-hidden="true"></i><span>City Master</span>
</a>

<ul class="treeview-menu">
<li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='addnew_city'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=addnew_city">Add City</a></li>
<li <?php if($_REQUEST['control']=='regional_hierarchy' && $_REQUEST['task']=='show_city'){ ?>class="active"<?php } ?>><a href="index.php?control=regional_hierarchy&task=show_city">View City</a></li>

</ul>
</li>-->



<!--<li class="treeview <?php if($_REQUEST['control']=='category_master' && ($_REQUEST['task']=='addnew' || $_REQUEST['task']=='show' || $_REQUEST['task']=='sort_category')){ ?>active<?php } ?>">
<a href="javascript:;">
<i class="fa fa-users" aria-hidden="true"></i>
<span>Category Master</span>
</a>

<ul class="treeview-menu">
<li <?php if($_REQUEST['control']=='category_master' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=category_master&task=addnew">Add Category</a></li>
<li <?php if($_REQUEST['control']=='category_master' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=category_master&task=show">View Category</a></li>
<li <?php if($_REQUEST['control']=='category_master' && $_REQUEST['task']=='sort_category'){ ?>class="active"<?php } ?>><a href="index.php?control=category_master&task=sort_category">Sort Category</a></li>

</ul>
</li>-->

		

         

        
        
         
         
        
</ul>
</section>
<!-- /.sidebar -->
</aside>



