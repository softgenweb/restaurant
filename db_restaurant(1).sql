-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2019 at 02:03 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE IF NOT EXISTS `activity_log` (
`id` int(11) NOT NULL,
  `system_ip` varchar(200) NOT NULL,
  `activity` varchar(220) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `system_ip`, `activity`, `user_id`, `date_created`, `status`) VALUES
(1, '192.168.1.34', 'Login by EMP004', 3, '2019-01-23 15:22:21', 1),
(2, '192.168.1.34', 'Login by admin', 1, '2019-01-23 15:26:33', 1),
(3, '192.168.1.30', 'Add Item Category (fghfh) by admin', 1, '2019-01-23 16:22:09', 1),
(4, '192.168.1.35', 'Login by admin', 1, '2019-01-24 10:41:13', 1),
(5, '192.168.1.35', 'Update Company Details by admin', 1, '2019-01-24 10:42:20', 1),
(6, '::1', 'Login by admin', 1, '2019-01-24 10:45:47', 1),
(7, '::1', 'Add New Bill (KSFW1819001) of total amount:126.00', 1, '2019-01-24 11:19:22', 1),
(8, '::1', 'Add New Bill (KSFW1819002) of total amount:262.50', 1, '2019-01-24 12:53:34', 1),
(9, '::1', 'Add New Bill (KSFW1819003) of total amount:262.50', 1, '2019-01-24 17:15:24', 1),
(10, '::1', 'Add New Bill (KSFW1819004) of total amount:242', 1, '2019-01-24 18:43:04', 1),
(11, '::1', 'Login by admin', 1, '2019-01-25 14:42:45', 1),
(12, '::1', 'Login by admin', 1, '2019-01-29 12:35:02', 1),
(13, '192.168.1.35', 'Login by admin', 1, '2019-02-04 18:08:49', 1),
(14, '192.168.1.35', 'Login by admin', 1, '2019-02-04 18:58:15', 1),
(15, '192.168.1.35', 'Login by admin', 1, '2019-02-04 19:01:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bill_discount`
--

CREATE TABLE IF NOT EXISTS `bill_discount` (
`id` int(11) NOT NULL,
  `percent` varchar(100) NOT NULL,
  `remark` varchar(225) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='bill discounts' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bill_discount`
--

INSERT INTO `bill_discount` (`id`, `percent`, `remark`, `date_created`, `status`) VALUES
(1, '0', '', '2018-12-19 11:13:01', 1),
(2, '6', '', '2018-12-19 11:13:42', 1),
(3, '12', '', '2018-12-19 11:13:53', 1),
(4, '15', '', '2018-12-19 11:13:53', 1),
(5, 'Other Amount', 'only for other amount', '2018-12-19 11:14:11', 1),
(6, '5', '', '2018-12-22 12:30:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bill_fare`
--

CREATE TABLE IF NOT EXISTS `bill_fare` (
`id` int(11) NOT NULL,
  `bill_no` varchar(220) NOT NULL,
  `token_no` int(11) NOT NULL,
  `customer_name` varchar(220) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `total_item` int(11) NOT NULL COMMENT 'number of total product/particuler',
  `total_qty` varchar(220) NOT NULL,
  `tax_percent` varchar(100) NOT NULL,
  `total_tax` varchar(200) NOT NULL,
  `disc_percent` varchar(100) NOT NULL,
  `total_discount` varchar(220) NOT NULL,
  `total_amount` varchar(220) NOT NULL,
  `grand_total` varchar(220) NOT NULL,
  `financial_year` varchar(220) NOT NULL,
  `payment_mode` int(11) NOT NULL,
  `card_number` bigint(100) NOT NULL COMMENT 'card last 4 number',
  `date_created` datetime NOT NULL,
  `date_modify` varchar(220) NOT NULL,
  `emp_id` bigint(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bill_fare`
--

INSERT INTO `bill_fare` (`id`, `bill_no`, `token_no`, `customer_name`, `mobile`, `dob`, `total_item`, `total_qty`, `tax_percent`, `total_tax`, `disc_percent`, `total_discount`, `total_amount`, `grand_total`, `financial_year`, `payment_mode`, `card_number`, `date_created`, `date_modify`, `emp_id`, `status`) VALUES
(1, 'KSFW1819001', 1, 'Nagesh', '9877777777', '', 1, '1', '5', '6.00', '0', '0.00', '120.00', '126.00', '181900', 3, 0, '2019-01-25 11:19:21', '2019-01-24 11:19:21', 1, 1),
(2, 'KSFW1819002', 2, 'Dfgdf', '9888888888', '10-10-2019', 2, '2', '5', '12.50', '0', '0.00', '250.00', '262.50', '181900', 2, 0, '2019-01-24 12:53:34', '2019-01-24 12:53:34', 1, 1),
(3, 'KSFW1819003', 3, 'Nagesh', '9888888888', '10-10-2019', 2, '2', '5', '12.50', '0', '0.00', '250.00', '262.50', '181900', 2, 0, '2019-01-24 17:15:23', '2019-01-24 17:15:23', 1, 1),
(4, 'KSFW1819004', 4, 'Dfgdf', '9888888888', '10-10-2019', 2, '2', '5', '11.50', '0', '0.00', '230.00', '242', '181900', 2, 0, '2019-01-24 18:43:03', '2019-01-24 18:43:03', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bill_items`
--

CREATE TABLE IF NOT EXISTS `bill_items` (
`id` bigint(100) NOT NULL,
  `bill_id` bigint(100) NOT NULL,
  `bill_no` varchar(220) NOT NULL,
  `cust_mobile` varchar(100) NOT NULL,
  `product_id` int(100) NOT NULL,
  `product_name` varchar(225) NOT NULL,
  `qty` int(220) NOT NULL,
  `rate` varchar(100) NOT NULL,
  `discount` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `remark` varchar(220) NOT NULL,
  `financial_year` varchar(100) NOT NULL,
  `bill_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `bill_items`
--

INSERT INTO `bill_items` (`id`, `bill_id`, `bill_no`, `cust_mobile`, `product_id`, `product_name`, `qty`, `rate`, `discount`, `amount`, `remark`, `financial_year`, `bill_date`, `date_created`, `status`) VALUES
(1, 1, 'KSFW1819001', '9877777777', 2, 'Regular (Masala Dosa)', 1, '120.00', '0.00', '120.00', '', '181900', '2019-01-24 11:19:22', '2019-01-24 11:19:22', 1),
(2, 2, 'KSFW1819002', '9888888888', 5, 'Aloo Veg Masala MIX Dosa', 1, '140.00', '0.00', '140.00', '', '181900', '2019-01-24 12:53:34', '2019-01-24 12:53:34', 1),
(3, 2, 'KSFW1819002', '9888888888', 3, 'Onion Dosa', 1, '110.00', '0.00', '110.00', '', '181900', '2019-01-24 12:53:34', '2019-01-24 12:53:34', 1),
(4, 3, 'KSFW1819003', '9888888888', 5, 'Aloo Veg Masala MIX Dosa', 1, '140.00', '0.00', '140.00', '', '181900', '2019-01-24 17:15:23', '2019-01-24 17:15:23', 1),
(5, 3, 'KSFW1819003', '9888888888', 3, 'Onion Dosa', 1, '110.00', '0.00', '110.00', '', '181900', '2019-01-24 17:15:23', '2019-01-24 17:15:23', 1),
(6, 4, 'KSFW1819004', '9888888888', 3, 'Onion Dosa', 1, '110.00', '0.00', '110.00', '', '181900', '2019-01-24 18:43:03', '2019-01-24 18:43:03', 1),
(7, 4, 'KSFW1819004', '9888888888', 4, 'Onion Masala Dosa', 1, '120.00', '0.00', '120.00', '', '181900', '2019-01-24 18:43:04', '2019-01-24 18:43:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bill_tax`
--

CREATE TABLE IF NOT EXISTS `bill_tax` (
`id` int(11) NOT NULL,
  `tax` varchar(100) NOT NULL,
  `percent` varchar(100) NOT NULL DEFAULT 'other',
  `remark` varchar(200) NOT NULL,
  `created_by` bigint(100) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `bill_tax`
--

INSERT INTO `bill_tax` (`id`, `tax`, `percent`, `remark`, `created_by`, `date_created`, `status`) VALUES
(1, 'GST', '5', '', 1, '2018-12-15 19:03:36', 1),
(2, 'GST', '0', '', 1, '2018-12-15 19:03:36', 1),
(3, 'GST', '12', '', 1, '2018-12-15 19:03:36', 1),
(4, 'OTHER AMOUNT', 'Other Amount', '', 1, '2018-12-15 19:07:07', 1),
(5, 'GST', '18', '', 1, '2018-12-22 12:20:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
`id` int(11) NOT NULL,
  `name` varchar(220) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `image` varchar(220) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `email`, `phone`, `mobile`, `address`, `gst_no`, `pan_no`, `image`, `date_created`, `date_modify`, `status`) VALUES
(1, 'Kavyashree Foodworks', 'gaurav.s@softgentechnologies.com', '0544623434', '8299431080', 'LDA Colony Near Power House Chauraha , Lucknow', '37ADAPM1724A2Z5', 'QWERT1234A', '1/1WhatsApp Image 2019-01-23 at 9.57.50 PM.jpeg', '2018-12-15 16:12:53', '2019-01-24 10:42:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `confic`
--

CREATE TABLE IF NOT EXISTS `confic` (
`id` int(11) NOT NULL,
  `confic_type_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `control` varchar(200) DEFAULT 'text'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `confic`
--

INSERT INTO `confic` (`id`, `confic_type_id`, `title`, `value`, `description`, `control`) VALUES
(1, 1, 'paging', '100', 'Page should be #5, 10, 20, 50, 100, 1000', 'select'),
(3, 1, 'default_mail', 'narai1987@gmail.com', 'Default mail to communicate with user', 'text'),
(4, 1, 'max_count', '100', '', ''),
(5, 2, 'language', '1', '', ''),
(6, 1, 'service_tax', '8', '', ''),
(10, 1, 'join_point', '100', '', ''),
(11, 1, 'rating_point', '10', '', ''),
(12, 1, 'get_point_on_purchase_product_per_1000_USD', '100', '', ''),
(13, 1, 'login_point', '5', '', ''),
(14, 1, 'price_per_100_point', '2', '', ''),
(15, 1, 'admin_gift_point', '50', '', ''),
(16, 1, 'minimum_used_point', '500', '', ''),
(17, 1, 'point_per_beverage', '5', '', ''),
(18, 1, 'point_per_eqipment', '5', '', ''),
(19, 1, 'point_per_food', '5', '', ''),
(20, 1, 'point_per_cabin', '10', '', ''),
(21, 1, 'trip_low_price_range', '5000', '', 'text'),
(22, 1, 'trip_high_price_range', '25000', '', 'text');

-- --------------------------------------------------------

--
-- Table structure for table `customer_details`
--

CREATE TABLE IF NOT EXISTS `customer_details` (
`id` bigint(100) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `name` varchar(220) NOT NULL,
  `mobile` varchar(220) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL COMMENT 'date_of_birth/ anniversery',
  `address` text NOT NULL,
  `date_created` datetime NOT NULL,
  `emp_id` bigint(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hrm_post`
--

CREATE TABLE IF NOT EXISTS `hrm_post` (
`id` int(11) NOT NULL,
  `post_name` varchar(220) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hrm_post`
--

INSERT INTO `hrm_post` (`id`, `post_name`, `date_created`, `status`) VALUES
(1, 'ADMIN', '2018-12-17 11:17:41', 1),
(2, 'MANAGER', '2018-12-17 11:46:51', 1),
(3, 'EMPLOYEE', '2018-12-17 11:47:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_mode`
--

CREATE TABLE IF NOT EXISTS `payment_mode` (
`id` int(11) NOT NULL,
  `mode` varchar(200) NOT NULL,
  `date_created` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `payment_mode`
--

INSERT INTO `payment_mode` (`id`, `mode`, `date_created`, `status`) VALUES
(1, 'DEBIT CARD', '2018-12-15 16:38:30', 1),
(2, 'CASH', '2018-12-15 18:43:51', 1),
(3, 'CREDIT CARD', '2018-12-15 18:44:17', 1),
(4, 'PAYTM', '2018-12-22 12:13:25', 1),
(5, 'GIFT CARD', '2018-12-27 13:34:39', 1),
(6, 'ZOMATO', '2018-12-27 13:35:26', 1),
(7, 'FOODPANDA', '2018-12-27 13:35:46', 1),
(8, 'GOOGLE PAY', '2018-12-27 13:36:09', 1),
(9, 'PHONEPE', '2018-12-27 13:36:24', 1),
(10, 'BHIM UPI', '2018-12-27 13:36:43', 1),
(11, 'COD', '2018-12-27 13:37:07', 1),
(12, 'SWIGGY', '2018-12-27 14:48:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
`id` int(11) NOT NULL,
  `name` varchar(220) NOT NULL,
  `remark` varchar(225) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `name`, `remark`, `date_created`, `status`) VALUES
(1, 'Combos', '', '2019-01-21 13:07:03', 1),
(2, 'Dosa', '', '2019-01-21 13:27:30', 1),
(3, 'Idli', '', '2019-01-21 13:27:41', 1),
(4, 'Gravy & Choice of Rice', '', '2019-01-21 13:27:41', 1),
(5, 'Snacks', '', '2019-01-21 13:27:41', 1),
(6, 'Beverages', '', '2019-01-21 13:27:41', 1),
(7, 'Ice Cream & Sweets', '', '2019-01-21 13:27:41', 1),
(8, 'fghfh', 'gfgh\r\n', '2019-01-23 16:22:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_list`
--

CREATE TABLE IF NOT EXISTS `product_list` (
`id` int(11) NOT NULL,
  `product_name` varchar(220) NOT NULL,
  `category_id` int(11) NOT NULL,
  `qty` varchar(100) NOT NULL DEFAULT '1',
  `price` varchar(100) NOT NULL,
  `discount_type` varchar(255) NOT NULL COMMENT 'Other Means manual Amount number means %',
  `discount` varchar(100) NOT NULL,
  `remark` varchar(220) NOT NULL,
  `details` varchar(255) NOT NULL COMMENT 'product Details',
  `date_created` datetime NOT NULL,
  `created_by` bigint(100) NOT NULL,
  `date_modify` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=133 ;

--
-- Dumping data for table `product_list`
--

INSERT INTO `product_list` (`id`, `product_name`, `category_id`, `qty`, `price`, `discount_type`, `discount`, `remark`, `details`, `date_created`, `created_by`, `date_modify`, `status`) VALUES
(1, 'Plain Dosa', 2, '1', '100', '0', '0', 'Regular', '', '2019-01-23 12:00:20', 1, '2019-01-23 12:00:20', 1),
(2, 'Masala Dosa', 2, '1', '120', '0', '0', 'Regular', '', '2019-01-23 12:00:21', 1, '2019-01-23 12:00:21', 1),
(3, 'Onion Dosa', 2, '1', '110', '0', '0', 'Regular', '', '2019-01-23 12:00:22', 1, '2019-01-23 12:00:22', 1),
(4, 'Onion Masala Dosa', 2, '1', '120', '0', '0', 'Regular', '', '2019-01-23 12:00:23', 1, '2019-01-23 12:00:23', 1),
(5, 'Aloo Veg Masala MIX Dosa', 2, '1', '140', '0', '0', 'Regular', '', '2019-01-23 12:00:24', 1, '2019-01-23 12:00:24', 1),
(6, 'Mysore Masala Dosa', 2, '1', '140', '0', '0', 'Regular', '', '2019-01-23 12:00:25', 1, '2019-01-23 12:00:25', 1),
(7, 'Mysore Paneer Dosa', 2, '1', '180', '0', '0', 'Regular', '', '2019-01-23 12:00:26', 1, '2019-01-23 12:00:26', 1),
(8, 'Paneer Dosa', 2, '1', '180', '0', '0', 'Regular', '', '2019-01-23 12:00:27', 1, '2019-01-23 12:00:27', 1),
(9, 'Paneer Masala Mix Dosa', 2, '1', '180', '0', '0', 'Regular', '', '2019-01-23 12:00:28', 1, '2019-01-23 12:00:28', 1),
(10, 'Set Dosa Dosa', 2, '1', '140', '0', '0', 'Regular', '', '2019-01-23 12:00:29', 1, '2019-01-23 12:00:29', 1),
(11, 'Palak Plain Dosa', 2, '1', '120', '0', '0', 'Green', '', '2019-01-23 12:00:30', 1, '2019-01-23 12:00:30', 1),
(12, 'Palak Masala Dosa', 2, '1', '140', '0', '0', 'Green', '', '2019-01-23 12:00:31', 1, '2019-01-23 12:00:31', 1),
(13, 'Green Peas Dosa', 2, '1', '180', '0', '0', 'Green', '', '2019-01-23 12:00:32', 1, '2019-01-23 12:00:32', 1),
(14, 'Green Capsicum Dosa', 2, '1', '140', '0', '0', 'Green', '', '2019-01-23 12:00:33', 1, '2019-01-23 12:00:33', 1),
(15, 'Green Coriander Dosa', 2, '1', '140', '0', '0', 'Green', '', '2019-01-23 12:00:34', 1, '2019-01-23 12:00:34', 1),
(16, 'Chineese Schezwan Dosa', 2, '1', '140', '0', '0', 'Chineese', '', '2019-01-23 12:00:35', 1, '2019-01-23 12:00:35', 1),
(17, 'Chineese Veg Manchurian Dosa', 2, '1', '180', '0', '0', 'Chineese', '', '2019-01-23 12:00:36', 1, '2019-01-23 12:00:36', 1),
(18, 'Chineese Spring Wrap Dosa', 2, '1', '180', '0', '0', 'Chineese', '', '2019-01-23 12:00:37', 1, '2019-01-23 12:00:37', 1),
(19, 'Chineese Chilli Paneer Dosa', 2, '1', '180', '0', '0', 'Chineese', '', '2019-01-23 12:00:38', 1, '2019-01-23 12:00:38', 1),
(20, 'Peanut Plain Dosa', 2, '1', '140', '0', '0', 'Special', '', '2019-01-23 12:00:39', 1, '2019-01-23 12:00:39', 1),
(21, 'Pav Bhaji Masala Dosa', 2, '1', '180', '0', '0', 'Special', '', '2019-01-23 12:00:40', 1, '2019-01-23 12:00:40', 1),
(22, 'Chana Dal fry Dosa', 2, '1', '180', '0', '0', 'Special', '', '2019-01-23 12:00:41', 1, '2019-01-23 12:00:41', 1),
(23, 'Corn fry Dosa', 2, '1', '180', '0', '0', 'Special', '', '2019-01-23 12:00:42', 1, '2019-01-23 12:00:42', 1),
(24, 'Mushroom fry Dosa', 2, '1', '180', '0', '0', 'Special', '', '2019-01-23 12:00:43', 1, '2019-01-23 12:00:43', 1),
(25, 'Garlic roasted Dosa', 2, '1', '140', '0', '0', 'Special', '', '2019-01-23 12:00:44', 1, '2019-01-23 12:00:44', 1),
(26, 'Twin Tower Dosa', 2, '1', '180', '0', '0', 'Special', '', '2019-01-23 12:00:45', 1, '2019-01-23 12:00:45', 1),
(27, 'Fusion Mix Veg Dosa', 2, '1', '220', '0', '0', 'Fusion', '', '2019-01-23 12:00:46', 1, '2019-01-23 12:00:46', 1),
(28, 'Fusion Mushroom Dosa', 2, '1', '220', '0', '0', 'Fusion', '', '2019-01-23 12:00:47', 1, '2019-01-23 12:00:47', 1),
(29, 'Fusin Corn Dosa', 2, '1', '220', '0', '0', 'Fusion', '', '2019-01-23 12:00:48', 1, '2019-01-23 12:00:48', 1),
(30, 'Rawa Plain Dosa', 2, '1', '100', '0', '0', 'Regular', '', '2019-01-23 12:00:49', 1, '2019-01-23 12:00:49', 1),
(31, 'Rawa Masala Dosa', 2, '1', '120', '0', '0', 'Regular', '', '2019-01-23 12:00:50', 1, '2019-01-23 12:00:50', 1),
(32, 'Rawa Onion Dosa', 2, '1', '110', '0', '0', 'Regular', '', '2019-01-23 12:00:51', 1, '2019-01-23 12:00:51', 1),
(33, 'Rawa Onion Masala Dosa', 2, '1', '120', '0', '0', 'Regular', '', '2019-01-23 12:00:52', 1, '2019-01-23 12:00:52', 1),
(34, 'Rawa Aloo Veg Masala MIX Dosa', 2, '1', '140', '0', '0', 'Regular', '', '2019-01-23 12:00:53', 1, '2019-01-23 12:00:53', 1),
(35, 'Rawa Palak Plain Dosa', 2, '1', '120', '0', '0', 'Green', '', '2019-01-23 12:00:54', 1, '2019-01-23 12:00:54', 1),
(36, 'Rawa Palak Masala Dosa', 2, '1', '140', '0', '0', 'Green', '', '2019-01-23 12:00:55', 1, '2019-01-23 12:00:55', 1),
(37, 'Rawa Green Peas Dosa', 2, '1', '180', '0', '0', 'Green', '', '2019-01-23 12:00:56', 1, '2019-01-23 12:00:56', 1),
(38, 'Rawa Capsicum Dosa', 2, '1', '140', '0', '0', 'Green', '', '2019-01-23 12:00:57', 1, '2019-01-23 12:00:57', 1),
(39, 'Rawa Coriander Dosa', 2, '1', '140', '0', '0', 'Green', '', '2019-01-23 12:00:58', 1, '2019-01-23 12:00:58', 1),
(40, 'Rawa Peanut Plain Dosa', 2, '1', '140', '0', '0', 'Special', '', '2019-01-23 12:00:59', 1, '2019-01-23 12:00:59', 1),
(41, 'Rawa Chana Dal fry Dosa', 2, '1', '180', '0', '0', 'Special', '', '2019-01-23 12:00:59', 1, '2019-01-23 12:00:60', 1),
(42, 'Rawa Corn fry Dosa', 2, '1', '180', '0', '0', 'Special', '', '2019-01-23 12:00:59', 1, '2019-01-23 12:00:61', 1),
(43, 'Rawa Mushroom fry Dosa', 2, '1', '180', '0', '0', 'Special', '', '2019-01-23 12:00:59', 1, '2019-01-23 12:00:62', 1),
(44, 'Rawa Garlic roasted Dosa', 2, '1', '140', '0', '0', 'Special', '', '2019-01-23 12:00:59', 1, '2019-01-23 12:00:63', 1),
(45, 'Rawa Mix Veg Dosa', 2, '1', '220', '0', '0', 'Fusion', '', '2019-01-23 12:00:59', 1, '2019-01-23 12:00:64', 1),
(46, 'Rice Idli or Vada,Plain Masala Dosa', 1, '1', '150', '0', '0', 'Combo1', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(47, 'Shahi Chole Bhatura with Butter milk', 1, '1', '160', '0', '0', 'Combo2', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(48, 'Shahi Cholley kulcha with Butter milk', 1, '1', '140', '0', '0', 'Combo3', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(49, 'Kurkure Chaat with Tea/Normal Coffee', 1, '1', '100', '0', '0', 'Combo4', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(50, 'Basket Chaat with Tea/Normal Coffee', 1, '1', '110', '0', '0', 'Combo5', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(51, 'Sarso Ka Saag, Makke ki Roti & Gud', 1, '1', '180', '0', '0', 'Combo6', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(52, 'Ghar Ki Thali', 1, '1', '150', '0', '0', 'Combo7', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(53, 'Special Thali', 1, '1', '220', '0', '0', 'Combo8', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(54, 'Punjabi Thali', 1, '1', '220', '0', '0', 'Combo9', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(55, 'Rajasthani Thali', 1, '1', '220', '0', '0', 'Combo10', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(56, 'Rice Idli or Vada,Any Special Dosa', 1, '1', '220', '0', '0', 'Combo11', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(57, 'Green Thali', 1, '1', '250', '0', '0', 'Combo12', '', '2019-01-23 12:00:59', 1, '23-01-2019 12:00', 1),
(58, 'Chole Bhature', 5, '1', '119', '0', '0', 'Snacks', 'Bathura, Chole garnished with Coriander and Garlic, Beetroot Onion, Green Chilli Masala Stuffed', '2019-01-21 13:07:03', 1, '23-01-2019 12:00', 1),
(59, 'Shahi Chole Bhature', 5, '1', '139', '0', '0', 'Snacks', 'Paneer Bathura, Chole with Malai Kofta garnished with Coriander and Garlic, Beetroot Onion, Green Chilli Masala Stuffed', '2019-01-21 13:07:04', 1, '23-01-2019 12:00', 1),
(60, 'Cholley kulcha', 5, '1', '109', '0', '0', 'Snacks', 'kulcha, Chole garnished with Coriander and Garlic, Beetroot Onion, Green Chilli Masala Stuffed', '2019-01-21 13:07:05', 1, '23-01-2019 12:00', 1),
(61, 'Shahi Cholley kulcha', 5, '1', '119', '0', '0', 'Snacks', 'kulcha, Chole with Malai Kofta garnished with Coriander and Garlic, Beetroot Onion, Green Chilli Masala Stuffed', '2019-01-21 13:07:06', 1, '23-01-2019 12:00', 1),
(62, 'Dahi Vada', 5, '1', '79', '0', '0', 'Snacks', '', '2019-01-21 13:07:07', 1, '23-01-2019 12:00', 1),
(63, 'Dahi Bhalla', 5, '1', '79', '0', '0', 'Snacks', '', '2019-01-21 13:07:08', 1, '23-01-2019 12:00', 1),
(64, 'Kurkure Chaat', 5, '1', '80', '0', '0', 'Snacks', '', '2019-01-21 13:07:09', 1, '23-01-2019 12:00', 1),
(65, 'Basket Chaat', 5, '1', '90', '0', '0', 'Snacks', '', '2019-01-21 13:07:10', 1, '23-01-2019 12:00', 1),
(66, 'Pav (1Pcs)', 5, '1', '20', '0', '0', 'Snacks', '', '2019-01-21 13:07:11', 1, '23-01-2019 12:00', 1),
(67, 'Special Pav Bhaji (2Pcs)', 5, '1', '110', '0', '0', 'Snacks', '', '2019-01-21 13:07:12', 1, '23-01-2019 12:00', 1),
(68, 'Vegetables Oat Chila', 5, '1', '99', '0', '0', 'South Indian', '', '2019-01-21 13:07:13', 1, '23-01-2019 12:00', 1),
(69, 'fv special Cheese Veg Tikki', 5, '1', '99', '0', '0', 'Italian', '', '2019-01-21 13:07:14', 1, '23-01-2019 12:00', 1),
(70, 'fv special Arrabbiata Veg (In Red Sauce)', 5, '1', '170', '0', '0', 'Italian', '', '2019-01-21 13:07:15', 1, '23-01-2019 12:00', 1),
(71, 'fv special Alfredo Veg. (In White Sauce)', 5, '1', '170', '0', '0', 'Italian', '', '2019-01-21 13:07:16', 1, '23-01-2019 12:00', 1),
(72, 'fv special Veggie Parma Rosa Pasta Bowl (Mix of White and Red Sauce)', 5, '1', '190', '0', '0', 'Italian', '', '2019-01-21 13:07:17', 1, '23-01-2019 12:00', 1),
(73, 'Stuffed Mushroom', 5, '1', '99', '0', '0', 'Italian', '', '2019-01-21 13:07:18', 1, '23-01-2019 12:00', 1),
(74, 'Chilli Potato', 5, '1', '90', '0', '0', 'Chineese', '', '2019-01-21 13:07:19', 1, '23-01-2019 12:00', 1),
(75, 'Honey Chilli Potato', 5, '1', '110', '0', '0', 'Chineese', '', '2019-01-21 13:07:20', 1, '23-01-2019 12:00', 1),
(76, 'Chilli Paneer', 5, '1', '170', '0', '0', 'Chineese', '', '2019-01-21 13:07:21', 1, '23-01-2019 12:00', 1),
(77, 'Chilli Mushroom', 5, '1', '180', '0', '0', 'Chineese', '', '2019-01-21 13:07:22', 1, '23-01-2019 12:00', 1),
(78, 'Veg Hakka Noodles', 5, '1', '140', '0', '0', 'Chineese', '', '2019-01-21 13:07:23', 1, '23-01-2019 12:00', 1),
(79, 'Chilli Garlic Noodles', 5, '1', '160', '0', '0', 'Chineese', '', '2019-01-21 13:07:24', 1, '23-01-2019 12:00', 1),
(80, 'Singapore Noodles', 5, '1', '180', '0', '0', 'Chineese', '', '2019-01-21 13:07:25', 1, '23-01-2019 12:00', 1),
(81, 'Schezwan Noodles', 5, '1', '140', '0', '0', 'Chineese', '', '2019-01-21 13:07:26', 1, '23-01-2019 12:00', 1),
(82, 'Kind Pao Veg Noodles', 5, '1', '199', '0', '0', 'Chineese', '', '2019-01-21 13:07:27', 1, '23-01-2019 12:00', 1),
(83, 'Rice Idli Sambar (2 Pcs)', 3, '1', '90', '0', '0', 'Idli', 'Dip idli in Sambar', '2019-01-23 12:00:20', 1, '2019-01-23 12:00:20', 1),
(84, 'Rice Idli Sambar (4 Pcs)', 3, '1', '145', '0', '0', 'Idli', 'Dip idli in Sambar', '2019-01-23 12:00:21', 1, '2019-01-23 12:00:21', 1),
(85, 'Sambar Vada', 3, '1', '90', '0', '0', 'Idli', 'Idli Vada in Sambar', '2019-01-23 12:00:22', 1, '2019-01-23 12:00:22', 1),
(86, 'Dahi Vada', 3, '1', '90', '0', '0', 'Idli', 'Idli in Dahi with Imli Chatni, Chaat Masala etc.', '2019-01-23 12:00:23', 1, '2019-01-23 12:00:23', 1),
(87, 'Beetroot Idli Sambar', 3, '1', '100', '0', '0', 'Idli', 'Stuffed, Stuffed Beetroot in Idli Batter while steam', '2019-01-23 12:00:24', 1, '2019-01-23 12:00:24', 1),
(88, 'Paneer Idli Sambar', 3, '1', '120', '0', '0', 'Idli', 'Stuffed', '2019-01-23 12:00:25', 1, '2019-01-23 12:00:25', 1),
(89, 'Capsicum Idli Sambar', 3, '1', '100', '0', '0', 'Idli', 'Stuffed', '2019-01-23 12:00:26', 1, '2019-01-23 12:00:26', 1),
(90, 'Green Peas Idli Sambar', 3, '1', '100', '0', '0', 'Idli', 'Stuffed', '2019-01-23 12:00:27', 1, '2019-01-23 12:00:27', 1),
(91, 'Palak Idli Sambar', 3, '1', '100', '0', '0', 'Idli', 'Stuffed', '2019-01-23 12:00:28', 1, '2019-01-23 12:00:28', 1),
(92, 'Onion Idli Sambar', 3, '1', '100', '0', '0', 'Idli', 'Fried, fry idli with Onion and spread with masala for taste. Serve seperately with Sambar', '2019-01-23 12:00:29', 1, '2019-01-23 12:00:29', 1),
(93, 'Garlic Idli Sambar', 3, '1', '100', '0', '0', 'Idli', 'Stuffed, Garlic idli should be serve fried', '2019-01-23 12:00:30', 1, '2019-01-23 12:00:30', 1),
(94, 'Coriander Idli Sambar', 3, '1', '100', '0', '0', 'Idli', 'Stuffed', '2019-01-23 12:00:31', 1, '2019-01-23 12:00:31', 1),
(95, 'Schezwan Idli Sambar', 3, '1', '120', '0', '0', 'Idli', 'Fried', '2019-01-23 12:00:32', 1, '2019-01-23 12:00:32', 1),
(96, 'Peanut Idli Sambar', 3, '1', '120', '0', '0', 'Idli', 'Stuffed, Peanut Idli shoul be served fried', '2019-01-23 12:00:33', 1, '2019-01-23 12:00:33', 1),
(97, 'Mysore Masala Idli', 3, '1', '120', '0', '0', 'Idli', 'Fried, Onion or Garlic idle can mix with Mysore Masala and served', '2019-01-23 12:00:34', 1, '2019-01-23 12:00:34', 1),
(98, 'Chana Daal Idli Sambar', 3, '1', '120', '0', '0', 'Idli', 'Stuffed, Stuffed Chana Daal with Idli and fry before serve', '2019-01-23 12:00:35', 1, '2019-01-23 12:00:35', 1),
(99, 'Corn Idli Sambar', 3, '1', '120', '0', '0', 'Idli', 'Stuffed', '2019-01-23 12:00:36', 1, '2019-01-23 12:00:36', 1),
(100, 'Idli Manchurian', 3, '1', '149', '0', '0', 'Idli', 'Fried', '2019-01-23 12:00:37', 1, '2019-01-23 12:00:37', 1),
(101, 'Idli Chat', 3, '1', '149', '0', '0', 'Idli', 'Fried', '2019-01-23 12:00:38', 1, '2019-01-23 12:00:38', 1),
(102, 'Dal vada', 3, '1', '90', '0', '0', 'Idli', 'Fried, chana dal onion coriender ginger and green chilli mess and deep fry in oil searve sambar and coconut chutni', '2019-01-23 12:00:39', 1, '2019-01-23 12:00:39', 1),
(103, 'Bisible Bath Papad (250 Gms)', 4, '1', '120', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:20', 1, '2019-01-23 12:00:20', 1),
(104, 'Bisible Bath (400 Gms)', 4, '1', '180', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:21', 1, '2019-01-23 12:00:21', 1),
(105, 'Curd Rice (250 Gms)', 4, '1', '120', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:22', 1, '2019-01-23 12:00:22', 1),
(106, 'Curd Rice (400 Gms)', 4, '1', '180', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:23', 1, '2019-01-23 12:00:23', 1),
(107, 'Lemon Rice (250 gms)', 4, '1', '120', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:24', 1, '2019-01-23 12:00:24', 1),
(108, 'Lemon Rice (400 gms)', 4, '1', '180', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:25', 1, '2019-01-23 12:00:25', 1),
(109, 'Rajma Rice with Raita', 4, '1', '150', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:26', 1, '2019-01-23 12:00:26', 1),
(110, 'Special Cholley Rice with Raita', 4, '1', '150', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:27', 1, '2019-01-23 12:00:27', 1),
(111, 'Shahi Kadi Rice with Raita', 4, '1', '150', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:28', 1, '2019-01-23 12:00:28', 1),
(112, 'Paneer Makhni Rice with Raita', 4, '1', '180', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:29', 1, '2019-01-23 12:00:29', 1),
(113, 'Cholley (100 Gms)', 4, '1', '70', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:30', 1, '2019-01-23 12:00:30', 1),
(114, 'Cholley (250 Gms)', 4, '1', '160', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:31', 1, '2019-01-23 12:00:31', 1),
(115, 'Rajma (100 Gms)', 4, '1', '90', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:32', 1, '2019-01-23 12:00:32', 1),
(116, 'Rajma (250 Gms)', 4, '1', '180', '0', '0', 'Gravy & Choice of Rice', '', '2019-01-23 12:00:33', 1, '2019-01-23 12:00:33', 1),
(117, 'Filter Coffee', 6, '1', '50', '0', '0', 'Beverages', '', '2019-01-23 12:00:20', 1, '2019-01-23 12:00:20', 1),
(118, 'Coffee Strong (150 ml)', 6, '1', '50', '0', '0', 'Beverages', '', '2019-01-23 12:00:21', 1, '2019-01-23 12:00:21', 1),
(119, 'Normal Coffee (150 ml)', 6, '1', '40', '0', '0', 'Beverages', '', '2019-01-23 12:00:22', 1, '2019-01-23 12:00:22', 1),
(120, 'Hot Tea (150 ml)', 6, '1', '30', '0', '0', 'Beverages', '', '2019-01-23 12:00:23', 1, '2019-01-23 12:00:23', 1),
(121, 'Sweet Lassi (250 ml)', 6, '1', '80', '0', '0', 'Beverages', '', '2019-01-23 12:00:24', 1, '2019-01-23 12:00:24', 1),
(122, 'Butter Milk (250 ml)', 6, '1', '60', '0', '0', 'Beverages', '', '2019-01-23 12:00:25', 1, '2019-01-23 12:00:25', 1),
(123, 'Butterscotch Milkshake (300 ml)', 6, '1', '90', '0', '0', 'Beverages', '', '2019-01-23 12:00:26', 1, '2019-01-23 12:00:26', 1),
(124, 'Strawberry Milkshake (300 ml)', 6, '1', '90', '0', '0', 'Beverages', '', '2019-01-23 12:00:27', 1, '2019-01-23 12:00:27', 1),
(125, 'Chocolate Milkshake (300 ml)', 6, '1', '90', '0', '0', 'Beverages', '', '2019-01-23 12:00:28', 1, '2019-01-23 12:00:28', 1),
(126, 'Cold Coffee (250 ml)', 6, '1', '90', '0', '0', 'Beverages', '', '2019-01-23 12:00:29', 1, '2019-01-23 12:00:29', 1),
(127, 'Ice tea (250ml)', 6, '1', '60', '0', '0', 'Beverages', '', '2019-01-23 12:00:30', 1, '2019-01-23 12:00:30', 1),
(128, 'Lemonade (300 ml)', 6, '1', '60', '0', '0', 'Beverages', '', '2019-01-23 12:00:31', 1, '2019-01-23 12:00:31', 1),
(129, 'Gulab Jamun (1 Pcs)', 7, '1', '30', '0', '0', 'Ice Cream & Sweets', '', '2019-01-23 12:00:20', 1, '2019-01-23 12:00:20', 1),
(130, 'Gulab Jamun (2 Pcs)', 7, '1', '50', '0', '0', 'Ice Cream & Sweets', '', '2019-01-23 12:00:21', 1, '2019-01-23 12:00:21', 1),
(131, 'Caf? Special Green Sweet (100 gm)', 7, '1', '70', '0', '0', 'Ice Cream & Sweets', '', '2019-01-23 12:00:22', 1, '2019-01-23 12:00:22', 1),
(132, 'Caf? Special Green Sweet (250 gm)', 7, '1', '120', '0', '0', 'Ice Cream & Sweets', '', '2019-01-23 12:00:23', 1, '2019-01-23 12:00:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
`id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `tmp_type` int(11) DEFAULT NULL,
  `default_temp` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `name`, `tmp_type`, `default_temp`, `status`) VALUES
(1, 'jet_tmp', 1, 0, 1),
(2, 'api', 0, 0, 1),
(7, 'kartiano_web', 1, 1, 1),
(8, 'restaurant', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `state_id` varchar(255) DEFAULT NULL,
  `city_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(255) DEFAULT NULL,
  `post` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `reporting` varchar(255) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `utype` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `mobile`, `email`, `state_id`, `city_id`, `department_id`, `post`, `reporting`, `image`, `date`, `utype`, `status`) VALUES
(1, 'admin', 'admin', 'Admin', '9411950511', 'admin@gmail.com', '', '1', '1', '1', '', '1/1imagePenguins.jpg', '01/02/2017', 'Administrator', 1),
(2, 'EMP001', 'admin', 'Nagesh rai', '8382936646', 'nagesh.k@softgentechnologies.com', '4', '2', '1', '2', '1', '', '', 'Employee', 1),
(3, 'EMP002', '123456', 'Gaurav Kumar Singh', '73763860056', 'gaurav.s@softgentechnologies.com', '', '', '', '3', '', '', '2018-12-17', 'Employee', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_discount`
--
ALTER TABLE `bill_discount`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_fare`
--
ALTER TABLE `bill_fare`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_items`
--
ALTER TABLE `bill_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_tax`
--
ALTER TABLE `bill_tax`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `confic`
--
ALTER TABLE `confic`
 ADD PRIMARY KEY (`id`,`confic_type_id`);

--
-- Indexes for table `customer_details`
--
ALTER TABLE `customer_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hrm_post`
--
ALTER TABLE `hrm_post`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_mode`
--
ALTER TABLE `payment_mode`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_list`
--
ALTER TABLE `product_list`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `bill_discount`
--
ALTER TABLE `bill_discount`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bill_fare`
--
ALTER TABLE `bill_fare`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `bill_items`
--
ALTER TABLE `bill_items`
MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bill_tax`
--
ALTER TABLE `bill_tax`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `confic`
--
ALTER TABLE `confic`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `customer_details`
--
ALTER TABLE `customer_details`
MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hrm_post`
--
ALTER TABLE `hrm_post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment_mode`
--
ALTER TABLE `payment_mode`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `product_list`
--
ALTER TABLE `product_list`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
